import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth.service';
import {FlashMessagesService} from 'angular2-flash-messages/module/flash-messages.service';

@Injectable()
export class UnauthenticatedGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private flashMessagesService: FlashMessagesService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin(state.url);
  }

  private checkLogin(url: string): boolean {
    console.log('Checking login for route ', url, ' - must be unauthenticated');
    if (this.authService.hasValidAccessToken()) {
      console.log('[GUARD] Login check successful, redirecting to dashboard');
      this.router.navigateByUrl('/dashboard').then(() => {
        this.flashMessagesService.show('You are already logged in.', {cssClass: 'alert-danger'});
      });
    } else {
      console.log('[GUARD] Login check unsuccessful, staying on login page');
      this.authService.clearAuthData();
      return true;
    }
  }
}
