import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';

@Injectable()
export class GitlabLoginGuard implements CanActivate {
  constructor(private authService: AuthService,
              private router: Router,
              private cookies: CookieService,
              private flashMessagesService: FlashMessagesService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.checkCookies()) {
      console.log('[GUARD] Found required cookies for Gitlab');
      return true;
    } else {
      console.log('[GUARD] Did not find required cookies');
      this.router.navigateByUrl('/login').then(() => {
        this.flashMessagesService.show('Invalid gitlab login attempt.', { cssClass: 'alert-danger' });
      });
    }
  }

  checkCookies() {
    console.log('[GUARD] Checking presence of cookies in gitlab login guard');
    const gitlab_token = this.cookies.get('gitlab_token');
    const username = this.cookies.get('username');
    console.log('[GUARD] Found cookies: token=', gitlab_token, ', username=', username);
    return !(gitlab_token === '' || username === '');
  }
}
