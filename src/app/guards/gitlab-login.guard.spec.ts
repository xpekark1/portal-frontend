import { TestBed, async, inject } from '@angular/core/testing';

import { GitlabLoginGuard } from './gitlab-login.guard';

describe('GitlabLoginGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GitlabLoginGuard]
    });
  });

  it('should ...', inject([GitlabLoginGuard], (guard: GitlabLoginGuard) => {
    expect(guard).toBeTruthy();
  }));
});
