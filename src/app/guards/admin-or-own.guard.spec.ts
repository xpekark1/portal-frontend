import { TestBed, async, inject } from '@angular/core/testing';

import { AdminOrOwnGuard } from './admin-or-own.guard';

describe('AdminOrOwnGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminOrOwnGuard]
    });
  });

  it('should ...', inject([AdminOrOwnGuard], (guard: AdminOrOwnGuard) => {
    expect(guard).toBeTruthy();
  }));
});
