import { TestBed, inject } from '@angular/core/testing';

import {UserListResolver} from './user-list-resolver.service';

describe('UserListResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserListResolver]
    });
  });

  it('should be created', inject([UserListResolver], (service: UserListResolver) => {
    expect(service).toBeTruthy();
  }));
});
