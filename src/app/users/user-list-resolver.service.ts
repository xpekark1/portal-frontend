import { Injectable } from '@angular/core';
import { User } from '../shared/models/models';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { take } from 'rxjs/operators';

@Injectable()
export class UserListResolver implements Resolve<User[]> {

  constructor(private service: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User[]> | Promise<User[]> | User[] {
    return this.service.listUsers().pipe(take(1));
  }
}
