import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {User} from '../../shared/models/models';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {
  users: User[];
  temp: User[];
  subscriptions: Subscription[] = [];

  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { users: User[] }) => {
      this.users = data.users;
      this.temp = [...data.users];
      console.log('[USERS] Users List: ', this.users);
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.username.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.users = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
