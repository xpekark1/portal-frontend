import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

import { UserCreateComponent } from './user-create/user-create.component';
import { UserPasswordComponent } from './user-password/user-password.component';
import { AdminGuard } from '../guards/admin.guard';
import { AdminOrOwnGuard } from '../guards/admin-or-own.guard';
import { UserResolver } from './user-resolver.service';
import { UserListResolver } from './user-list-resolver.service';
import { LoggedInUserResolver } from '../resolvers/logged-in-user-resolver.service';


const routes: Routes = [
  {
    path: '',
    component: UserListComponent,
    pathMatch: 'full',
    canActivate: [AdminGuard],
    resolve: {
      users: UserListResolver
    }
  },
  {
    path: 'create',
    component: UserCreateComponent,
    canActivate: [AdminGuard]
  },
  {
    path: ':uid',
    component: UserProfileComponent,
    canActivate: [AdminOrOwnGuard],
    runGuardsAndResolvers: 'always',
    resolve: {
      user: UserResolver,
      loggedInUser: LoggedInUserResolver
    }
  },
  {
    path: ':uid/password',
    component: UserPasswordComponent,
    canActivate: [AdminOrOwnGuard],
    resolve: {
      user: UserResolver,
      loggedInUser: LoggedInUserResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
