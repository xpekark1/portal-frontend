import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../shared/models/models';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: [ './user-profile.component.scss' ]
})
export class UserProfileComponent implements OnInit, OnDestroy {

  user: User; // the user whose profile is being viewed
  loggedInUser: User; // the user who is viewing the profile
  own: boolean;
  updateData: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private service: UserService,
              public auth: AuthService,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private flashMessagesService: FlashMessagesService) {
    this.createForm();
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { user: User, loggedInUser: User }) => {
      this.user = data.user;
      this.loggedInUser = data.loggedInUser;
      this.own = (this.user.id === this.loggedInUser.id);
      this.initFormValues();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initFormValues() {
    console.log('setting values for user profile form');
    this.updateData.setValue({
      username: this.user.username,
      email: this.user.email,
      uco: this.user.uco,
      name: this.user.name || '',
      is_admin: this.user.is_admin,
      created_at: this.datePipe.transform(this.user.created_at, 'yyyy-MM-ddTHH:mm'),
      updated_at: this.datePipe.transform(this.user.updated_at, 'yyyy-MM-ddTHH:mm'),
    });
  }

  createForm() {
    this.updateData = this.fb.group({
      username: [ '', Validators.required ],
      email: [ '', [ Validators.required, Validators.email ] ],
      uco: undefined,
      name: undefined,
      is_admin: undefined,
      created_at: undefined,
      updated_at: undefined,
    });
  }

  updateProfile() {
    console.log('updating user profile');
    if (this.updateData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    const data = this.updateData.value;
    const newValues = {
      id: this.user.id,
      username: data[ 'username' ],
      email: data[ 'email' ],
      uco: data[ 'uco' ],
      name: data[ 'name' ],
      is_admin: data[ 'is_admin' ]
    } as User;
    this.subscriptions.push(this.service.updateUser(newValues).subscribe(
      res => {
        this.ngOnInit();
        this.flashMessagesService.show('Update successful.', { cssClass: 'alert-success' });
      }, (error: any) => {
        this.flashMessagesService.show(`Update unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }
    ));
  }

  deleteUser() {
    const userId = this.user.id;
    this.subscriptions.push(this.service.deleteUser(userId).subscribe(
      res => {
        console.log('Deleted user ', userId);
        this.router.navigateByUrl('/users').then(() => {
          this.flashMessagesService.show(`Deleted user ${userId}`, { cssClass: 'alert-success' });
        });

      }, (error: any) => {
        this.flashMessagesService.show(`Delete unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }));
  }

  resetForm() {
    this.updateData.reset();
    this.initFormValues();
  }
}
