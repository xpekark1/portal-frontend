import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {UserService} from '../../services/user.service';
import {FlashMessagesService} from 'angular2-flash-messages/module/flash-messages.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  createData: FormGroup;
  constructor(private userService: UserService,
              private router: Router,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService) {
    this.createForm();
  }

  createForm() {
    this.createData = this.fb.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      uco: ['', Validators.required],
      is_admin: [false],
    });
  }

  ngOnInit() { }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    console.log('User create form data: ', data);
    this.userService.createUser(data['username'], data['name'], data['email'], data['uco'], data['is_admin']).subscribe(
      () => {
      this.router.navigateByUrl('/users').then(() => {
        this.flashMessagesService.show(`Created user ${data['username']}.`, {cssClass: 'alert-success'});
      });
    }, (error: any) => {
        console.log('Error creating user: ', error);
        this.flashMessagesService.show(`Error creating user ${data['username']}.`, {cssClass: 'alert-danger', timeout: 10000});
        console.log('Response getPortalStatus code: ', error.status);
        console.log('Response data: ', error.error);
        this.flashMessagesService.show(`${error.error.message}`, {cssClass: 'alert-danger', timeout: 10000});
      });
  }
}
