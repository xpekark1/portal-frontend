import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { User } from '../shared/models/models';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';

import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { map, take } from 'rxjs/operators';

@Injectable()
export class UserResolver implements Resolve<User> {

  constructor(private service: UserService, private router: Router, private flashMessagesService: FlashMessagesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    const uid = route.paramMap.get('uid');

    return this.service.getUser(uid).pipe(take(1), map(user => {
        if (user) {
          return user;
        } else {
          this.router.navigateByUrl('/').then(
            () => {
              this.flashMessagesService.show(`User ${uid} not found.`, { cssClass: 'alert-danger' });
            }, (error: any) => {
              console.log('Could not navigate home: ', error.error);
            });
          return null;
        }
      })
    );
  }
}
