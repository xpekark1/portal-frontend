import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserListComponent } from './user-list/user-list.component';
import { UsersRoutingModule } from './users-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserPasswordComponent } from './user-password/user-password.component';
import { UserResolver } from './user-resolver.service';
import { UserListResolver } from './user-list-resolver.service';
import { WorkersModule } from '../workers/workers.module';
import { SecretsModule } from '../secrets/secrets.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    WorkersModule,
    SecretsModule,
    NgxDatatableModule
  ],
  declarations: [
    UserProfileComponent,
    UserListComponent,
    UserCreateComponent,
    UserPasswordComponent
  ],
  providers: [
    UserResolver,
    UserListResolver
  ],
  exports: [
    UserProfileComponent,
    UserListComponent
  ]
})
export class UsersModule {
}
