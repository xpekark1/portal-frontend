export class CopyConstructable {
  constructor(obj: Object = null) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}

export class TimedMixin extends CopyConstructable {
  public id: string;
  public created_at: Date = null;
  public updated_at: Date = null;
}


export class NamedMixin extends TimedMixin {
  public name: string;
  public codename: string;
  public description: string;
}



