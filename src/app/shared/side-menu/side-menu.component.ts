import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { User } from '../models/models';
import { Subscription } from 'rxjs';

/**
 * Models the sub-menus off the main left-hand vertical nav.
 */
export enum SideSubmenus {
  None, Dashboard, Users, Workers, Courses, Submissions
}

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: [ './side-menu.component.scss' ]
})
export class SideMenuComponent implements OnInit, OnDestroy {

  public submenuTypes: any = SideSubmenus;
  public currentSubmenu: SideSubmenus = this.submenuTypes.None;
  public subMenuOut = false;

  loggedInUser: User;
  subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, public router: Router, public auth: AuthService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
    }));

    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.hideSubmenu();
      }
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * Navigates the user to the Dashboard.
   */
  public goToDashboard(): void {
    this.router.navigate([ '/dashboard' ]);
  }


  /**
   * Returns true if the given route is the active route.
   * @param route the expected route
   * @param exact should the route be exact match?
   *
   */
  public isRouteActive(route: string, exact: boolean = false): boolean {
    return this.router.isActive(route, exact);
  }

  /**
   * Returns true if the currently active route is the dashboard.
   * @returns {boolean}
   */
  isDashboardRoute(): boolean {
    return this.isRouteActive('/dashboard', true);
  }

  /**
   * Returns true if the currently active route is /users/*
   * @returns {boolean}
   */
  isUsersRoute() {
    return this.isRouteActive('/users');
  }

  /**
   * Returns true if the currently active route is /courses/*
   * @returns {boolean}
   */
  isCoursesRoute() {
    return this.isRouteActive('/courses');

  }

  /**
   * Returns true if the currently active route is /submissions/*
   * @returns {boolean}
   */
  isSubmissionsRoute() {
    return this.isRouteActive('/submissions');
  }

  /**
   * Returns true if the currently active route is /workers/*
   * @returns {boolean}
   */
  isWorkersRoute() {
    return this.isRouteActive('/workers');
  }

  /**
   * Called when the user clicks the vertical menu shade (the grey shaded area behind the submenu div that
   * is displayed when a sub-menu is selected).  Clicking the shade makes the sub-menu div go away.
   */
  hideSubmenu(): void {
    this.subMenuOut = false;
    this.currentSubmenu = SideSubmenus.None;
  }

  /**
   * Toggles a sub-menu off the main vertical left-hand menu bar. If the sub-menu is
   * already selected, it de-selects it.
   * @param subMenu the sub-menu to toggle
   */
  toggleSubMenu(subMenu: SideSubmenus): void {
    if (this.subMenuOut && this.currentSubmenu === subMenu) {
      this.hideSubmenu();
    } else {
      this.currentSubmenu = subMenu;
      this.subMenuOut = true;
    }
  }
}

