import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { User } from '../models/models';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { ManagementService } from '../../services/management.service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: [ './top-menu.component.scss' ]
})
export class TopMenuComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  @Input() status = false;
  subscriptions: Subscription[] = [];


  constructor(private route: ActivatedRoute,
              private auth: AuthService,
              private router: Router,
              private managementService: ManagementService,
              private flashMessagesService: FlashMessagesService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  logout() {
    console.log('Logging out user ', this.loggedInUser.id);
    this.auth.logout();
    this.router.navigateByUrl('/login').then(() => {
      this.flashMessagesService.show('You have been logged out.', { cssClass: 'alert-success' });
    });
  }

}
