import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Submission, User } from '../models/models';
import { ActivatedRoute, Router } from '@angular/router';
import { SubmissionService } from '../../services/submission.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Subscription } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-submissions-table',
  templateUrl: './submissions-table.component.html',
  styleUrls: [ './submissions-table.component.scss' ]
})
export class SubmissionsTableComponent implements OnInit, OnDestroy {
  @Input() submissions: Submission[];
  @Input() loggedInUser: User;
  @Input() config: any = {};
  @Input() limit = 5;

  subscriptions: Subscription[] = [];
  temp: Submission[];

  loadingIndicator = true;
  reorderable = true;
  @ViewChild(DatatableComponent) table: DatatableComponent;

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  constructor(private route: ActivatedRoute,
              private submissionService: SubmissionService,
              private flashMessagesService: FlashMessagesService,
              private router: Router) {
  }

  ngOnInit() {
    this.temp = [ ...this.submissions ];
    console.log('user submissions:', this.submissions);
  }

  reloadSubmissions() {
    this.loadingIndicator = true;
    const submission_list = this.submissionService.listSubmissions({ user: this.loggedInUser.id }).subscribe(
      (submissions: Submission[]) => {
        this.submissions = submissions;
        this.loggedInUser.submissions = submissions;
        this.loadingIndicator = false;
      });
    this.subscriptions.push(submission_list);
  }

  cancelSubmission(submission_id: string) {
    this.subscriptions.push(this.submissionService.cancelSubmission(submission_id).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(
          () => {
            this.flashMessagesService.show('Submission cancelled.', { cssClass: 'alert-success' });
          }, (error: any) => {
            console.log('Navigation failed: ', error);
            this.flashMessagesService.show('Navigation failed, try navigating elsewhere.', { cssClass: 'alert-danger' });
          }
        );
      }
    ));
  }

  getRowClass = (row: any) => {
    const rowResult = row.result;
    const classResultName = `row-class-${rowResult}`;
    const result = {};
    result[ classResultName ] = true;
    return result;
  };
}
