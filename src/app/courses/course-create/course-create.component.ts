import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages/module/flash-messages.service';
import {CourseService} from '../../services/course.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-course-create',
  templateUrl: './course-create.component.html',
  styleUrls: ['./course-create.component.scss']
})
export class CourseCreateComponent implements OnInit, OnDestroy {

  createData: FormGroup;
  subscriptions: Subscription[] = [];
  constructor(private courseService: CourseService,
              private router: Router,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService) {
    this.createForm();
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.createData = this.fb.group({
      name: ['', Validators.required],
      codename: ['', Validators.required],
      description: [''],
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    console.log('[CREATE] Course create form data: ', data);
    this.subscriptions.push(this.courseService.createCourse(data['name'], data['codename'], data['description']).subscribe(
      () => {
        this.router.navigateByUrl('/courses').then(() => {
          this.flashMessagesService.show(`Created course ${data['codename']}.`, {cssClass: 'alert-success'});
        });
      }, (error: any) => {
        console.error('Error creating course: ', error);
        this.flashMessagesService.show(`Error creating course ${data['codename']}.`, {cssClass: 'alert-danger', timeout: 10000});
        console.error('Response data: ', error.error);
        this.flashMessagesService.show(`${error.error.message}`, {cssClass: 'alert-danger', timeout: 10000});
      }));
  }
}
