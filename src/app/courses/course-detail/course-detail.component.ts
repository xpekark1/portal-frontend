import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../../services/course.service';
import { Course, User } from '../../shared/models/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs';
import { PermissionsService } from '../../services/permissions.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: [ './course-detail.component.scss' ]
})
export class CourseDetailComponent implements OnInit, OnDestroy {
  course: Course;
  loggedInUser: User;
  projectsExpanded = false;
  rolesExpanded = false;
  groupsExpanded = false;
  updateFormData: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute,
              private service: CourseService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              public auth: AuthService,
              private router: Router,
              public permissions: PermissionsService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      this.initFormValues();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }


  initFormValues() {
    console.log('setting values for course detail form');
    console.log('have course: ', this.course);
    this.updateFormData.setValue({
      name: this.course.name,
      codename: this.course.codename,
      description: this.course.description,
    });
  }

  createForm() {
    this.updateFormData = this.fb.group({
      name: [ '', Validators.required ],
      codename: [ '', Validators.required ],
      description: [ '' ],
    });
  }

  toggleProjectList() {
    if (this.course.projects == null) {
      this.subscriptions.push(this.service.loadProjectsForCourse(this.course).subscribe());
    }
    this.projectsExpanded = !this.projectsExpanded;
  }

  toggleRoleList() {
    if (this.course.roles == null) {
      this.subscriptions.push(this.service.loadRolesForCourse(this.course).subscribe());
    }
    this.rolesExpanded = !this.rolesExpanded;
  }

  toggleGroupList() {
    if (this.course.groups == null) {
      this.subscriptions.push(this.service.loadGroupsForCourse(this.course).subscribe());
    }
    this.groupsExpanded = !this.groupsExpanded;
  }

  updateCourse() {
    if (this.updateFormData.status !== 'VALID') {
      console.warn('[COURSE] Invalid form submitted', this.updateFormData);
      return;
    }
    const data = this.updateFormData.value;
    const newValues = {
      id: this.course.id,
      name: data[ 'name' ],
      codename: data[ 'codename' ],
      description: data[ 'description' ],
    } as Course;
    const updateCourseSubscription = this.service.updateCourse(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flashMessagesService.show('Update successful.', { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        this.flashMessagesService.show(`Update unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }
    );
    this.subscriptions.push(updateCourseSubscription);
  }

  deleteCourse() {
    const courseId = this.course.id;
    this.subscriptions.push(this.service.deleteCourse(courseId).subscribe(
      () => {
        console.log('Deleted course ', courseId);
        this.router.navigateByUrl('/courses').then(() => {
          this.flashMessagesService.show(`Deleted course ${courseId}`, { cssClass: 'alert-success' });
        });

      }, (error: any) => {
        this.flashMessagesService.show(`Delete unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }));
  }

  resetForm() {
    this.updateFormData.reset();
    this.initFormValues();
  }

  userCanUpdateCourse() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }
}
