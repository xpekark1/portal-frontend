import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Group } from '../../shared/models/models';
import { Observable } from 'rxjs';
import { GroupService } from '../../services/group.service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { map, take } from 'rxjs/operators';

@Injectable()
export class GroupResolver implements Resolve<Group> {

  constructor(public service: GroupService,
              private router: Router,
              private flashMessagesService: FlashMessagesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Group> | Promise<Group> | Group {
    console.log('[RESOLVE] route data in group resolver: ', route.data);
    const course = route.parent.data[ 'course' ];
    const gid = route.paramMap.get('gid');
    return this.service.findGroup(course.id, gid).pipe(take(1), map(role => {
      if (role) {
        return role;
      }
      this.router.navigateByUrl(`/courses/${course.id}`).then(
        () => {
          this.flashMessagesService.show(`Group ${gid} not found in course ${course.id}.`, { cssClass: 'alert-danger' });
        }, (error: any) => {
          console.error(`[RESOLVE] Could not navigate to course ${course.id}: `, error.error);
        });
      return null;
    }));
  }
}
