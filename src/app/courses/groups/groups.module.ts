import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupCreateComponent } from './group-create/group-create.component';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { GroupListComponent } from './group-list/group-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GroupService } from '../../services/group.service';
import { GroupResolver } from './group-resolver.service';
import { GroupsRoutingModule } from './groups-routing.module';
import { GroupUsersComponent } from './group-users/group-users.component';
import { GroupProjectsComponent } from './group-projects/group-projects.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    GroupsRoutingModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    GroupService,
    GroupResolver
  ],
  declarations: [
    GroupCreateComponent,
    GroupDetailComponent,
    GroupListComponent,
    GroupUsersComponent,
    GroupProjectsComponent
  ]
})
export class GroupsModule {
}
