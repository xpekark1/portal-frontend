import { NgModule } from '@angular/core';
import { CourseResolver } from '../course-resolver.service';
import { LoggedInUserResolver } from '../../resolvers/logged-in-user-resolver.service';
import { RouterModule, Routes } from '@angular/router';
import { GroupListComponent } from './group-list/group-list.component';
import { GroupCreateComponent } from './group-create/group-create.component';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { GroupResolver } from './group-resolver.service';
import { RoleResolver } from '../roles/role-resolver.service';
import { RoleUsersComponent } from '../roles/role-users/role-users.component';
import { GroupUsersComponent } from './group-users/group-users.component';
import { Group } from '../../shared/models/models';
import { GroupProjectsComponent } from './group-projects/group-projects.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: GroupListComponent
  },
  {
    path: 'create',
    pathMatch: 'full',
    component: GroupCreateComponent
  },
  {
    path: ':gid',
    runGuardsAndResolvers: 'always',
    component: GroupDetailComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver,
      group: GroupResolver,
    }
  },
  {
    path: ':gid/users',
    runGuardsAndResolvers: 'always',
    component: GroupUsersComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver,
      group: GroupResolver,
    }
  },
  {
    path: ':gid/projects',
    runGuardsAndResolvers: 'always',
    component: GroupProjectsComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver,
      group: GroupResolver,
    }
  },
];


@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})

export class GroupsRoutingModule {
}
