import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { AuthService } from '../../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PermissionsService } from '../../../services/permissions.service';
import { GroupService } from '../../../services/group.service';
import { Subscription } from 'rxjs';
import { Course, User } from '../../../shared/models/models';

@Component({
  selector: 'app-group-create',
  templateUrl: './group-create.component.html',
  styleUrls: [ './group-create.component.scss' ]
})
export class GroupCreateComponent implements OnInit, OnDestroy {

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: GroupService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  loggedInUser: User;
  course: Course;
  createData: FormGroup;
  subscriptions: Subscription[] = [];


  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      // route guard based on user's permissions not usable here, see
      // https://github.com/angular/angular/issues/20805
      if (!this.userCanCreateGroup()) {
        this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
            this.flashMessagesService.show('You are not authorized to create new groups.', { cssClass: 'alert-danger' });
          }
        );
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.createData = this.fb.group({
      name: [ '', Validators.required ],
      codename: [ '', Validators.required ],
      description: [ '' ],
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.warn('[FORM] invalid form submitted');
      return;
    }
    console.log('[FORM] Project create form data: ', data);
    const createRoleSubscription = this.service.createGroup(this.course.id, data[ 'name' ], data[ 'description' ], data[ 'codename' ])
      .subscribe(
      () => {
        this.router.navigateByUrl(`/courses/${this.course.id}/groups`).then(() => {
          this.flashMessagesService.show(`Created group ${data[ 'name' ]}.`, { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        console.error('Error creating group: ', error);
        this.flashMessagesService.show(`Error creating role ${data[ 'name' ]}.`, { cssClass: 'alert-danger', timeout: 10000 });
        console.error('Response data: ', error.error);
        this.flashMessagesService.show(`${error.error.message}`, { cssClass: 'alert-danger', timeout: 10000 });
      });
    this.subscriptions.push(createRoleSubscription);
  }


  userCanCreateGroup() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }


}
