import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Course, Group, User } from '../../../shared/models/models';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { PermissionsService } from '../../../services/permissions.service';
import { GroupService } from '../../../services/group.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: [ './group-list.component.scss' ]
})
export class GroupListComponent implements OnInit, OnDestroy {

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public groupService: GroupService,
              private permissions: PermissionsService) {
  }

  loggedInUser: User;
  course: Course;
  groups: Group[];
  temp: Group[];
  subscriptions: Subscription[] = [];

  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      this.groups = this.course.groups;
      this.temp = [];
      this.loadGroups();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  userCanCreateGroup() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }

  loadGroups() {
    const listSubscription = this.groupService.findGroups(this.course.id);
    this.subscriptions.push(
      listSubscription.subscribe((groups) => {
        this.course.groups = groups;
        this.groups = groups;
        this.temp = [ ...groups ];
        console.log('set course.groups to ', groups);
      })
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    // update the rows
    this.groups = this.temp.filter(function (d) {
      return d.codename.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  userCanUpdateUsers() {
    return this.permissions.checkWriteGroups(this.loggedInUser, this.course);
  }

  userCanUpdateProjects() {
    return this.permissions.checkWriteGroups(this.loggedInUser, this.course);
  }
}
