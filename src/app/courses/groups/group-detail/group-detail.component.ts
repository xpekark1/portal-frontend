import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Group, Role, User } from '../../../shared/models/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { AuthService } from '../../../services/auth.service';
import { PermissionsService } from '../../../services/permissions.service';
import { GroupService } from '../../../services/group.service';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: [ './group-detail.component.scss' ]
})
export class GroupDetailComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  group: Group;
  formData: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: GroupService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, group: Group }) => {
      this.loggedInUser = data.loggedInUser;
      this.group = data.group;
      this.initFormValues();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initFormValues() {
    console.log('[FORM] initializing form values');
    if (this.group == null) {
      console.log('group was == null');
      return;
    }
    console.log('[FORM] setting values for group detail form');
    this.formData.setValue({
      name: this.group.name,
      description: this.group.description,
      codename: this.group.codename,
    });
  }

  createForm() {
    this.formData = this.fb.group({
      name: [ '', Validators.required ],
      codename: [ '', Validators.required ],
      description: [ '' ],
    });
  }

  resetForm() {
    this.formData.reset();
    this.initFormValues();
  }

  userCanUpdateGroup() {
    return this.permissions.checkWriteGroups(this.loggedInUser, this.group.course);
  }

  updateGroup() {
    console.log('[FORM] updating Form Group');
    if (this.formData.status !== 'VALID') {
      console.warn('[FORM]invalid form submitted');
      return;
    }
    const data = this.formData.value;
    const newValues = {
      id: this.group.id,
      course: this.group.course,
      name: data[ 'name' ],
      codename: data[ 'codename' ],
      description: data[ 'description' ],
    } as Group;
    const updateGroupSubscription = this.service.updateGroup(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flashMessagesService.show('Update successful.', { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        this.flashMessagesService.show(`Update unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }
    );
    this.subscriptions.push(updateGroupSubscription);
  }

  deleteGroup() {
    const deleteGroupSubscription = this.service.deleteGroup(this.group.course.id, this.group.id).subscribe(
      () => {
        console.log('Deleted group ', this.group.id);
        this.router.navigateByUrl(`/courses/${this.group.course.id}/group`).then(() => {
          this.flashMessagesService.show(`Deleted role ${this.group.id}`, { cssClass: 'alert-success' });
        });

      }, (error: any) => {
        this.flashMessagesService.show(`Delete unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      });
    this.subscriptions.push(deleteGroupSubscription);
  }
}
