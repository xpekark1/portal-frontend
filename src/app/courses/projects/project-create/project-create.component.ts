import { Component, OnDestroy, OnInit } from '@angular/core';
import { Course, User } from '../../../shared/models/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { ProjectService } from '../../../services/project.service';
import { Subscription } from 'rxjs';
import { PermissionsService } from '../../../services/permissions.service';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: [ './project-create.component.scss' ]
})
export class ProjectCreateComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  course: Course;
  createData: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public projectService: ProjectService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      // route guard based on user's permissions not usable here, see
      // https://github.com/angular/angular/issues/20805
      if (!this.userCanCreateProject()) {
        this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
            this.flashMessagesService.show('You are not authorized to create new projects.', { cssClass: 'alert-danger' });
          }
        );
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.createData = this.fb.group({
      name: [ '', Validators.required ],
      description: [ '' ],
      codename: [ '', Validators.required ],
      assignment_url: [ '' ],
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    console.log('Project create form data: ', data);
    this.subscriptions.push(this.projectService.createProject(this.course.id, data[ 'name' ], data['description'],
      data[ 'codename' ], data['assignment_url']).subscribe(
      () => {
        this.router.navigateByUrl(`/courses/${this.course.id}/projects`).then(() => {
          this.flashMessagesService.show(`Created project ${data[ 'codename' ]}.`, { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        console.log('Error creating group: ', error);
        this.flashMessagesService.show(`Error creating project ${data[ 'codename' ]}.`, { cssClass: 'alert-danger', timeout: 10000 });
        console.log('Response data: ', error.error);
        this.flashMessagesService.show(`${error.error.message}`, { cssClass: 'alert-danger', timeout: 10000 });
      }));
  }

  userCanCreateProject() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }
}
