import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Course, Project, ProjectConfig, User } from '../../../shared/models/models';
import { ProjectService } from '../../../services/project.service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-project-config',
  templateUrl: './project-config.component.html',
  styleUrls: [ './project-config.component.scss' ]
})
export class ProjectConfigComponent implements OnInit {
  @Input() loggedInUser: User;
  @Input() project: Project;
  configData: FormGroup;
  allowedFromDate: Date;

  get course(): Course {
    return this.project.course;
  }

  constructor(private fb: FormBuilder,
              public projectService: ProjectService,
              private flashMessagesService: FlashMessagesService,
              private router: Router,
              private route: ActivatedRoute,
              private permissions: PermissionsService,
              private datePipe: DatePipe
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.initFormValues();
  }

  createForm() {
    this.configData = this.fb.group({
      testFilesSource: '',
      testFilesSubdir: '',
      fileWhitelist: '',
      preSubmitScript: '',
      postSubmitScript: '',
      submissionParameters: '',
      submissionSchedulerConfig: '',
      submissionsAllowedFrom: new Date(),
      submissionsAllowedTo: new Date(),
      archiveFrom: new Date(),
    });
  }

  initFormValues() {
    // TODO: revamp date -> date, time, validate input
    console.log('[Project] Project config: ', this.project.config);
    this.configData.patchValue({
      testFilesSource: this.project.config.test_files_source,
      testFilesSubdir: this.project.config.test_files_subdir,
      fileWhitelist: this.project.config.file_whitelist,
      preSubmitScript: this.project.config.pre_submit_script,
      postSubmitScript: this.project.config.post_submit_script,
      submissionParameters: this.project.config.submission_parameters,
      submissionSchedulerConfig: this.project.config.submission_scheduler_config,
      submissionsAllowedFrom: new Date(this.project.config.submissions_allowed_from),
      submissionsAllowedTo: new Date(this.project.config.submissions_allowed_to),
      archiveFrom: new Date(this.project.config.archive_from),
    });
  }

  updateConfig() {
    console.log('updating group config');
    if (this.configData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    const data = this.configData.value;
    const newValues = {
      project: this.project,
      test_files_source: data[ 'testFilesSource' ],
      test_files_subdir: data[ 'testFilesSubdir' ],
      file_whitelist: data[ 'fileWhitelist' ],
      pre_submit_script: data[ 'preSubmitScript' ],
      post_submit_script: data[ 'postSubmitScript' ],
      submission_parameters: data[ 'submissionParameters' ],
      submission_scheduler_config: data[ 'submissionSchedulerConfig' ],
      submissions_allowed_from: data[ 'submissionsAllowedFrom' ].toISOString(),
      submissions_allowed_to: data[ 'submissionsAllowedTo' ].toISOString(),
      archive_from: data[ 'archiveFrom' ].toISOString()
    } as ProjectConfig;
    this.projectService.updateProjectConfig(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route, preserveQueryParams: true }).then(() => {
          this.flashMessagesService.show('Configuration update successful.', { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        this.flashMessagesService.show(`Configuration update unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }
    );
  }

  resetForm() {
    this.configData.reset();
    this.initFormValues();
  }

  userCanViewFullConfig() {
    return this.permissions.checkReadProjects(this.loggedInUser, this.course);
  }

  userCanUpdateProject() {
    return this.permissions.checkWriteProjects(this.loggedInUser, this.project.course);
  }
}
