import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {of} from 'rxjs';

@Injectable()
export class ShowConfigAtLoadResolver implements Resolve<boolean> {

  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return of(route.queryParams['showConfigAtLoad']);
  }
}
