import { TestBed, inject } from '@angular/core/testing';
import {ProjectResolver} from './project-resolver.service';


describe('ProjectResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectResolver]
    });
  });

  it('should be created', inject([ProjectResolver], (service: ProjectResolver) => {
    expect(service).toBeTruthy();
  }));
});
