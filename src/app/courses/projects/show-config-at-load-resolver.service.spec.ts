import { TestBed, inject } from '@angular/core/testing';

import {ShowConfigAtLoadResolver} from './show-config-at-load-resolver.service';

describe('ShowConfigAtLoadResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowConfigAtLoadResolver]
    });
  });

  it('should be created', inject([ShowConfigAtLoadResolver], (service: ShowConfigAtLoadResolver) => {
    expect(service).toBeTruthy();
  }));
});
