import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProjectCreateComponent} from './project-create/project-create.component';
import {ProjectDetailComponent} from './project-detail/project-detail.component';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectResolver} from './project-resolver.service';
import {LoggedInUserResolver} from '../../resolvers/logged-in-user-resolver.service';
import {CourseResolver} from '../course-resolver.service';
import {ShowConfigAtLoadResolver} from './show-config-at-load-resolver.service';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ProjectListComponent
  },
  {
    path: 'create',
    pathMatch: 'full',
    component: ProjectCreateComponent
  },
  {
    path: ':pid',
    component: ProjectDetailComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver,
      project: ProjectResolver,
      showConfigAtLoad: ShowConfigAtLoadResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
