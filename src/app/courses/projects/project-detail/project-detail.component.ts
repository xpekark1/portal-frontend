import { Component, OnDestroy, OnInit } from '@angular/core';
import { Project, User } from '../../../shared/models/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { ProjectService } from '../../../services/project.service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { Subscription } from 'rxjs';
import { PermissionsService } from '../../../services/permissions.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: [ './project-detail.component.scss' ]
})
export class ProjectDetailComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  project: Project;
  formData: FormGroup;
  showConfig: boolean;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public projectService: ProjectService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  get course() {
    return this.project.course;
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, project: Project, showConfigAtLoad: boolean }) => {
      this.loggedInUser = data.loggedInUser;
      this.project = data.project;
      this.showConfig = data.showConfigAtLoad ? data.showConfigAtLoad : false;
      this.initFormValues();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initFormValues() {
    console.log('initializing form values');
    if (this.project == null) {
      console.log('project was == null');
      return;
    }
    this.formData.setValue({
      name: this.project.name,
      description: this.project.description,
      codename: this.project.codename,
      assignment_url: this.project.assignment_url
    });
  }

  createForm() {
    this.formData = this.fb.group({
      name: [ '', Validators.required ],
      codename: [ '', Validators.required ],
      description: [ '' ],
      assignment_url: [ '' ],
    });
  }

  updateProject() {
    console.log('updating project');
    if (this.formData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    const data = this.formData.value;
    const newValues = {
      id: this.project.id,
      course: this.project.course,
      name: data[ 'name' ],
      codename: data[ 'codename' ],
      description: data[ 'description' ],
      assignment_url: data[ 'assignment_url' ],
    } as Project;
    this.subscriptions.push(this.projectService.updateProject(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flashMessagesService.show('Update successful.', { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        this.flashMessagesService.show(`Update unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }
    ));
  }

  deleteProject() {
    const projectId = this.project.id;
    const deleteProjectSubscription = this.projectService.deleteProject(this.course.id, projectId).subscribe(
      () => {
        console.log('Deleted group ', projectId);
        this.router.navigateByUrl(`/courses/${this.course.id}/projects`).then(() => {
          this.flashMessagesService.show(`Deleted project ${projectId}`, { cssClass: 'alert-success' });
        });

      }, (error: any) => {
        this.flashMessagesService.show(`Delete unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      });
    this.subscriptions.push(deleteProjectSubscription);
  }

  resetForm() {
    this.formData.reset();
    this.initFormValues();
  }

  userCanUpdateProject() {
    console.log()
    return this.permissions.checkWriteProjects(this.loggedInUser, this.course);
  }

  userCanCreateSubmissions() {
    return this.permissions.checkAll(this.loggedInUser, this.course, [ 'create_submissions' ]);
  }

  updateTestFiles() {
    this.subscriptions.push(this.projectService.updateProjectFiles(this.course.id, this.project.id).subscribe(() => {}));
  }
}
