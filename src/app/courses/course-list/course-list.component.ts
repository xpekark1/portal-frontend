import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CourseService } from '../../services/course.service';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';
import { Course, User } from '../../shared/models/models';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: [ './course-list.component.scss' ]
})
export class CourseListComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  subscriptions: Subscription[] = [];
  courses: Course[];
  temp: Course[];

  loadingIndicator = true;
  reorderable = true;
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private route: ActivatedRoute,
              private auth: AuthService,
              private courseService: CourseService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
      this.courses = this.loggedInUser.courses;
      this.temp = [];
      this.checkCourses(this.loggedInUser);
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  checkCourses(user: User) {
    this.loadCourses(user);
  }

  private getListSubscription(user: User) {
    if (user.is_admin) {
      return this.courseService.listCourses();
    } else {
      return this.userService.loadCoursesForUser(user);
    }
  }

  loadCourses(user: User) {
    const listSubscription = this.getListSubscription(user);
    this.subscriptions.push(
      listSubscription.subscribe((courses) => {
        this.courses = courses;
        user.courses = courses;
        this.temp = [...courses];
        console.log('set user.courses to ', this.courses);
      })
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.codename.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.courses = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
