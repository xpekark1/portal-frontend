import { Component, OnDestroy, OnInit } from '@angular/core';
import { Project, Role, User } from '../../../shared/models/models';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { RoleService } from '../../../services/role.service';

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: [ './role-detail.component.scss' ]
})
export class RoleDetailComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  role: Role;
  formData: FormGroup;
  showConfigAtLoad: boolean;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: RoleService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, role: Role, showConfigAtLoad: boolean }) => {
      this.loggedInUser = data.loggedInUser;
      this.role = data.role;
      this.showConfigAtLoad = data.showConfigAtLoad ? data.showConfigAtLoad : false;
      this.initFormValues();
    }));
  }


  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  initFormValues() {
    console.log('initializing form values');
    if (this.role == null) {
      console.log('group was == null');
      return;
    }
    console.log('setting values for role detail form');
    this.formData.setValue({
      name: this.role.name,
      codename: this.role.codename,
      description: this.role.description,
    });
  }

  createForm() {
    this.formData = this.fb.group({
      name: [ '', Validators.required ],
      codename: [ '', Validators.required ],
      description: [ '' ],
    });
  }

  updateRole() {
    console.log('[FORM] updating Form Group');
    if (this.formData.status !== 'VALID') {
      console.warn('[FORM] Invalid form submitted');
      return;
    }
    const data = this.formData.value;
    const newValues = {
      id: this.role.id,
      course: this.role.course,
      name: data[ 'name' ],
      codename: data[ 'codename' ],
      description: data[ 'description' ],
    } as Role;
    const updateRoleSubscription = this.service.updateRole(newValues).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(() => {
          this.flashMessagesService.show('Update successful.', { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        this.flashMessagesService.show(`Update unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }
    );
    this.subscriptions.push(updateRoleSubscription);
  }

  deleteRole() {
    const deleteRoleSubscription = this.service.deleteRole(this.role.course.id, this.role.id).subscribe(
      () => {
        console.log('[FORM] Deleted group ', this.role.id);
        this.router.navigateByUrl(`/courses/${this.role.course.id}/roles`).then(() => {
          this.flashMessagesService.show(`Deleted role ${this.role.id}`, { cssClass: 'alert-success' });
        });

      }, (error: any) => {
        this.flashMessagesService.show(`Delete unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      });
    this.subscriptions.push(deleteRoleSubscription);
  }

  resetForm() {
    this.formData.reset();
    this.initFormValues();
  }

  userCanUpdateRole() {
    return this.permissions.checkWriteRoles(this.loggedInUser, this.role.course);
  }
}
