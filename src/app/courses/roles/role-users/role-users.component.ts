import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Course, Role, User } from '../../../shared/models/models';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { AuthService } from '../../../services/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RoleService } from '../../../services/role.service';
import { PermissionsService } from '../../../services/permissions.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-role-users',
  templateUrl: './role-users.component.html',
  styleUrls: [ './role-users.component.scss' ]
})
export class RoleUsersComponent implements OnInit, OnDestroy {

  loggedInUser: User;
  role: Role;
  subscriptions: Subscription[] = [];
  formData: FormGroup;

  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: RoleService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  ngOnInit() {
    const subscription = this.route.data.subscribe((data: { loggedInUser: User, role: Role }) => {
      if (!this.userCanAddUser(data.loggedInUser, data.role.course)) {
        this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
            this.flashMessagesService.show('You are not authorized to modify users in the list.', { cssClass: 'alert-danger' });
          }
        );
      }
      this.loggedInUser = data.loggedInUser;
      this.role = data.role;
      const roleUsersSubs = this.service.getClients(this.role).subscribe((clients) => {
        this.role.clients = clients;
      });
      this.subscriptions.push(roleUsersSubs);
    });
    this.subscriptions.push(subscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  doRemoveUser(userId: string) {
    const removeUserSub = this.service.removeUser(this.role, userId).subscribe(() => {
      this.router.navigateByUrl('.').then(() => {
        this.flashMessagesService.show(`Updated users list.`, { cssClass: 'alert-success' });
      }, (error: any) => {
        console.error('Error removing user ', error);
        this.flashMessagesService.show(`Error removing user: ${userId}.`, { cssClass: 'alert-danger', timeout: 10000 });
        this.flashMessagesService.show(`${error.error.message}`, { cssClass: 'alert-danger', timeout: 10000 });
      });
    });
    this.subscriptions.push(removeUserSub);
  }

  createForm() {
    this.formData = this.fb.group({
      username: [ '' ],
    });
  }

  userCanAddUser(user?: User, course?: Course) {
    user = user || this.loggedInUser;
    course = course || this.role.course;
    return this.permissions.checkWriteRoles(this.loggedInUser, this.role.course);
  }

  doAddUser() {
    const data = this.formData.value;
    if (this.formData.status !== 'VALID') {
      console.warn('invalid form submitted');
      return;
    }
    const addUserSub = this.service.addUser(this.role, data[ 'username' ]).subscribe(() => {
      this.router.navigateByUrl('.').then(() => {
        this.flashMessagesService.show(`Updated users list.`, { cssClass: 'alert-success' });
      }, (error: any) => {
        console.error('Error adding user ', error);
        this.flashMessagesService.show(`Error adding user: ${data[ 'username' ]}.`, { cssClass: 'alert-danger', timeout: 10000 });
        this.flashMessagesService.show(`${error.error.message}`, { cssClass: 'alert-danger', timeout: 10000 });
      });
    });
    this.subscriptions.push(addUserSub);
  }
}
