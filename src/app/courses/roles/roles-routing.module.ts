import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleCreateComponent } from './role-create/role-create.component';
import { RoleDetailComponent } from './role-detail/role-detail.component';
import { CourseResolver } from '../course-resolver.service';
import { LoggedInUserResolver } from '../../resolvers/logged-in-user-resolver.service';
import { RoleResolver } from './role-resolver.service';
import { RoleUsersComponent } from './role-users/role-users.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: RoleListComponent
  },
  {
    path: 'create',
    pathMatch: 'full',
    component: RoleCreateComponent
  },
  {
    path: ':rid',
    runGuardsAndResolvers: 'always',
    component: RoleDetailComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver,
      role: RoleResolver,
    }
  },
  {
    path: ':rid/users',
    runGuardsAndResolvers: 'always',
    component: RoleUsersComponent,
    resolve: {
      loggedInUser: LoggedInUserResolver,
      course: CourseResolver,
      role: RoleResolver,
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})

export class RolesRoutingModule {
}
