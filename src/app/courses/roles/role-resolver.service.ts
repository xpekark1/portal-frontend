import { Injectable } from '@angular/core';
import { Role } from '../../shared/models/models';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { RoleService } from '../../services/role.service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { map, take } from 'rxjs/operators';

@Injectable()
export class RoleResolver implements Resolve<Role> {

  constructor(public service: RoleService,
              private router: Router,
              private flashMessagesService: FlashMessagesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Role> | Promise<Role> | Role {
    console.log('[RESOLVE] route data in group resolver: ', route.data);
    const course = route.parent.data[ 'course' ];
    const rid = route.paramMap.get('rid');
    return this.service.findRole(course.id, rid).pipe(take(1), map(role => {
        if (role) {
          return role;
        }
        this.router.navigateByUrl(`/courses/${course.id}`).then(
          () => {
            this.flashMessagesService.show(`Role ${rid} not found in course ${course.id}.`, { cssClass: 'alert-danger' });
          }, (error: any) => {
            console.error(`[RESOLVE] Could not navigate to course ${course.id}: `, error.error);
          });
        return null;
      })
    );
  }

}
