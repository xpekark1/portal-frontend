import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleCreateComponent } from './role-create/role-create.component';
import { RoleDetailComponent } from './role-detail/role-detail.component';
import { RolesRoutingModule } from './roles-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RoleResolver } from './role-resolver.service';
import { RoleService } from '../../services/role.service';
import { RolePermissionsComponent } from './role-permissions/role-permissions.component';
import { RoleUsersComponent } from './role-users/role-users.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    RolesRoutingModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [
    RoleResolver,
    RoleService
  ],
  declarations: [
    RoleListComponent,
    RoleCreateComponent,
    RoleDetailComponent,
    RolePermissionsComponent,
    RoleUsersComponent
  ]
})
export class RolesModule {
}
