import { Component, OnDestroy, OnInit } from '@angular/core';
import { Course, User } from '../../../shared/models/models';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionsService } from '../../../services/permissions.service';
import { RoleService } from '../../../services/role.service';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: [ './role-create.component.scss' ]
})
export class RoleCreateComponent implements OnInit, OnDestroy {

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              public service: RoleService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              private permissions: PermissionsService) {
    this.createForm();
  }

  loggedInUser: User;
  course: Course;
  createData: FormGroup;
  subscriptions: Subscription[] = [];

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, course: Course }) => {
      this.loggedInUser = data.loggedInUser;
      this.course = data.course;
      // route guard based on user's permissions not usable here, see
      // https://github.com/angular/angular/issues/20805
      if (!this.userCanCreateRole()) {
        this.router.navigate([ '..' ], { relativeTo: this.route }).then(() => {
            this.flashMessagesService.show('You are not authorized to create new roles.', { cssClass: 'alert-danger' });
          }
        );
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.createData = this.fb.group({
      name: [ '', Validators.required ],
      codename: [ '', Validators.required ],
      description: [ '' ],
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.warn('invalid form submitted');
      return;
    }
    console.log('Project create form data: ', data);
    const createRoleSubscription = this.service.createRole(this.course.id, data[ 'name' ], data[ 'codename' ], data[ 'description' ])
      .subscribe(
      () => {
        this.router.navigateByUrl(`/courses/${this.course.id}/roles`).then(() => {
          this.flashMessagesService.show(`Created role ${data[ 'codename' ]}.`, { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        console.error('Error creating group: ', error);
        this.flashMessagesService.show(`Error creating role ${data[ 'codename' ]}.`, { cssClass: 'alert-danger', timeout: 10000 });
        console.error('Response data: ', error.error);
        this.flashMessagesService.show(`${error.error.message}`, { cssClass: 'alert-danger', timeout: 10000 });
      });
    this.subscriptions.push(createRoleSubscription);
  }

  userCanCreateRole() {
    return this.permissions.checkUpdateCourse(this.loggedInUser, this.course);
  }
}
