import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Client } from '../shared/models/models';
import { map, take } from 'rxjs/operators';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ClientService } from '../services/client.service';

@Injectable({
  providedIn: 'root'
})
export class ClientResolver {

  constructor(private service: ClientService,
              private router: Router,
              private flashMessagesService: FlashMessagesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Client> {
    const clientId = route.paramMap.get('clientId');
    return this.service.getClient(clientId).pipe(take(1), map(client => {
      if (client) {
        return client;
      } else {
        this.router.navigateByUrl('/').then(
          () => {
            this.flashMessagesService.show(`Client ${clientId} not found.`, { cssClass: 'alert-danger' });
          }, (error: any) => {
            console.error(`[RESOLVE] Could not navigate to ${route.pathFromRoot}: `, error.error);
          });
        return null;
      }
    }));
  }
}
