import { Injectable } from '@angular/core';
import {Course} from '../shared/models/models';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {CourseService} from '../services/course.service';
import { take } from 'rxjs/operators';

@Injectable()
export class CourseQueryParamResolver implements Resolve<Course>{

  constructor(private courseService: CourseService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Course> | Promise<Course> {
    console.log('in course query param resolver');
    const courseId = route.queryParams['cid'];
    if (courseId == null) {
      return null;
    }
    return this.courseService.getCourse(courseId).pipe(take(1));
  }

}
