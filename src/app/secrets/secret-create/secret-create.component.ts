import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { Subscription } from 'rxjs';
import { Client, User } from '../../shared/models/models';
import { SecretService } from '../../services/secret.service';
import { DatePipe, Location } from '@angular/common';

@Component({
  selector: 'app-secret-create',
  templateUrl: './secret-create.component.html',
  styleUrls: ['./secret-create.component.scss']
})
export class SecretCreateComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  target: Client;
  createData: FormGroup;
  subscriptions: Subscription[] = [];
  newSecretValue: string;
  showSecretModal = false;
  now = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');

  constructor(private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private service: SecretService,
              private datePipe: DatePipe,
              private flashMessagesService: FlashMessagesService,
              private location: Location) {
    this.createForm();
  }

  private static combineDateAndTime(date: string, time: string) {
    if ([null, '', undefined].includes(date) && [null, '', undefined].includes(time)) {
      return null;
    }
    return date + 'T' + time;
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, target: Client }) => {
      this.loggedInUser = data.loggedInUser;
      this.target = data.target;
      console.log('got user ', this.loggedInUser);
      console.log('got target ', this.target);
      if (!this.userCanCreateSecret()) {
        this.flashMessagesService.show(`You are not authorized to create a new secret for ${this.target.codename}.`,
          {cssClass: 'alert-danger'});
        this.location.back();
      }
    }));
    console.log(this.now);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.createData = this.fb.group({
      name: [ '', Validators.required ],
      expiresAtDate: [ '' ],
      expiresAtTime: [ '' ],
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID' || !this.validateDatetime()) {
      console.log('invalid form submitted');
      return;
    }
    console.log('Secret create form data: ', data);
    const datetime = SecretCreateComponent.combineDateAndTime(data['expiresAtDate'], data['expiresAtTime']);
    this.subscriptions.push(this.service.createSecret(this.target.id, data[ 'name' ], datetime).subscribe(
      (response) => {
        this.showNewSecretModal(response['value']);
      }, (error: any) => {
        console.log('Error creating secret: ', error);
        this.flashMessagesService.show(`Error creating secret ${data[ 'name' ]}.`, { cssClass: 'alert-danger', timeout: 10000 });
        console.log('Response data: ', error.error);
        this.flashMessagesService.show(`${error.error.message}`, { cssClass: 'alert-danger', timeout: 10000 });
      }));
  }

  userCanCreateSecret() {
    return this.loggedInUser.is_admin || this.loggedInUser.id === this.target.id;
  }

  showNewSecretModal(value: string) {
    console.log('should open modal');
    this.newSecretValue = value;
    this.showSecretModal = true;
    this.createData.reset();
  }

  goAway() {
    this.showSecretModal = false;
    this.location.back();
  }

  validateDatetime(): boolean {
    const data = this.createData.value;
    const date = data['expiresAtDate'];
    const time = data['expiresAtTime'];
    return([null, '', undefined].includes(date) && [null, '', undefined].includes(time))
      ||   (![null, '', undefined].includes(date) && ![null, '', undefined].includes(time));
  }
}
