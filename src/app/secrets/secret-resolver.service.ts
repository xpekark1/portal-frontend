import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Client, Secret } from '../shared/models/models';
import { Observable } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages';
import { map, take } from 'rxjs/operators';
import { SecretService } from '../services/secret.service';

@Injectable({
  providedIn: 'root'
})
export class SecretResolver implements Resolve<Secret> {

  constructor(private service: SecretService,
              private router: Router,
              private flashMessagesService: FlashMessagesService) {
  }

  private static getOwner(route: ActivatedRouteSnapshot): Client {
    const user = route.parent.data.get( 'user' ) as Client;
    const worker = route.parent.data.get( 'worker' ) as Client;

    if (user == null || worker == null) {
      console.log(`Parent route for secret does not contain possible owner.`); // todo: can this cast correctly?
      return null;
    }
    return user == null ? worker : user;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Secret> {
    const sid = route.paramMap.get('sid');

    const owner = SecretResolver.getOwner(route);

    return this.service.loadSecret(owner.id, sid).pipe(take(1), map(secret => {
      if (secret) {
        return secret;
      } else {
        this.router.navigateByUrl('/').then(
          () => {
            this.flashMessagesService.show(`Secret ${secret} not found.`, { cssClass: 'alert-danger' });
          }, (error: any) => {
            console.error(`[RESOLVE] Could not navigate to secret ${sid}: `, error.error);
          });
        return null;
      }
    }));
  }
}
