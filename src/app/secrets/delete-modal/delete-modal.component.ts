import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Secret, User } from '../../shared/models/models';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SecretService } from '../../services/secret.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit, OnDestroy {

  @Input() loggedInUser: User;
  @Input() secret: Secret;
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private flashMessagesService: FlashMessagesService,
              private service: SecretService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  doDelete() {
    const secretId = this.secret.id;
    console.log('have secret', this.secret);
    this.subscriptions.push(this.service.deleteSecret(this.secret.client.id, secretId).subscribe(
      () => {
        console.log(`Deleted secret ${secretId}`);
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate(['.'], { relativeTo: this.route }).then(() => {
          this.flashMessagesService.show(`Deleted secret '${this.secret.name}'`, { cssClass: 'alert-success' });
        });

      }, (error: any) => {
        this.flashMessagesService.show(`Delete unsuccessful: ${error.error}`, { cssClass: 'alert-danger' });
      }
    ));
  }
}
