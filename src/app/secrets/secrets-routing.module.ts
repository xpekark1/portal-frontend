import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecretCreateComponent } from './secret-create/secret-create.component';
import { LoggedInUserResolver } from '../resolvers/logged-in-user-resolver.service';
import { ClientResolver } from '../resolvers/client-resolver.service';
import { AdminOrOwnGuard } from '../guards/admin-or-own.guard';

const routes: Routes = [
  {
    path: 'create',
    component: SecretCreateComponent,
    canActivate: [ AdminOrOwnGuard ], // TODO check
    resolve: {
      loggedInUser: LoggedInUserResolver,
      target: ClientResolver,
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class SecretsRoutingModule {
}
