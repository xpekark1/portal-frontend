import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Client, Secret, User } from '../../shared/models/models';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { SecretService } from '../../services/secret.service';

@Component({
  selector: 'app-secret-list',
  templateUrl: './secret-list.component.html',
  styleUrls: ['./secret-list.component.scss'],
})
export class SecretListComponent implements OnInit, OnDestroy, OnChanges {

  @Input() loggedInUser: User;
  @Input() secretsOwner: Client;

  secrets: Secret[];
  subscriptions: Subscription[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private service: SecretService,
  ) {
  }

  ngOnInit() {
    this.loadSecrets();
    console.log('secretsOwner type: ' + this.secretsOwner.type);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngOnChanges(changes: SimpleChanges) {
    this.loadSecrets();
  }

  loadSecrets() {
    const listSubscription = this.service.loadSecrets(this.secretsOwner.id);
    this.subscriptions.push(
      listSubscription.subscribe((secrets) => {
        this.secrets = secrets;
        console.log('set secrets to ', secrets);
      })
    );
  }

}
