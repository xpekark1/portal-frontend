import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Secret } from '../shared/models/models';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SecretService {
  clientsUrl = `${environment.baseUrl}/api/v1.0/clients`;

  constructor(private http: HttpClient) { }

  public loadSecrets(clientId: string): Observable<Secret[]>  {
    const url = `${this.clientsUrl}/${clientId}/secrets`;
    console.log(`[SECRET] List secrets (${url})`);
    return this.http.get<Secret[]>(url);
  }

  public loadSecret(clientId: string, secretName: string): Observable<Secret>  {
    const url = `${this.clientsUrl}/${clientId}/secrets/${secretName}`;
    console.log(`[SECRET] Find secret (${url}, secret name: ${secretName})`);
    return this.http.get<Secret>(url);
  }

  public updateSecret(clientId: string, secretName: string, newSecret: Secret): Observable<Secret[]>  {
    const url = `${this.clientsUrl}/${clientId}/secrets/${secretName}`;
    const body = {
      'name': newSecret.name,
      'expires_at': newSecret.expires_at
    };
    console.log(`[SECRET] Update secret (${url}: ${body})`);
    return this.http.put<Secret[]>(url, body);
  }

  public deleteSecret(clientId: string, secretId: string) {
    const url = `${this.clientsUrl}/${clientId}/secrets/${secretId}`;
    console.log(`[SECRET] Delete secret (${url})`);
    return this.http.delete(url);
  }

  public createSecret(clientId: string, name: string, expiresAt) {
    const url = `${this.clientsUrl}/${clientId}/secrets`;
    console.log(`[SECRET] Create secret (${url})`);
    const body = {
      'name': name,
      'expires_at': expiresAt
    };
    return this.http.post(url, body);
  }
}
