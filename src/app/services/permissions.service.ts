import { Injectable } from '@angular/core';
import { Course, User } from '../shared/models/models';

@Injectable()
export class PermissionsService {

  constructor() {
  }

  public checkReadProjects(user: User, course: Course) {
    const required = [ 'update_course', 'read_course_full', 'write_projects' ];
    return this.checkAny(user, course, required);
  }


  checkReadResults(user: User, course: Course) {
    const required = [ 'update_course', 'read_course_full', 'read_all_submission_files' ];
    return this.checkAny(user, course, required);
  }

  public checkReadGroups(user: User, course: Course) {
    const required = [ 'update_course', 'read_course_full', 'write_projects' ];
    return this.checkAny(user, course, required);
  }

  public checkReadRoles(user: User, course: Course) {
    return this.checkAny(user, course, [ 'update_course', 'read_course_full', 'write_projects' ]);
  }

  public checkUpdateCourse(user: User, course: Course) {
    return this.checkAll(user, course, [ 'update_course' ]);
  }

  public checkWriteRoles(user: User, course: Course) {
    return this.checkAny(user, course, [ 'update_course', 'write_roles' ]);
  }

  public checkWriteProjects(user: User, course: Course) {
    return this.checkAny(user, course, [ 'update_course', 'write_projects' ]);
  }

  public checkWriteGroups(user: User, course: Course) {
    return this.checkAny(user, course, [ 'update_course', 'write_groups' ]);
  }

  checkReviewSubmission(user: User, course: Course): boolean {
    return this.checkAny(user, course,
      [ 'write_reviews_all', 'write_reviews_group', 'update_course' ]);
  }

  public checkAny(user: User, course: Course | string, required: string[]): boolean {
    if (user.is_admin) {
      return true;
    }
    const provided = (typeof course === 'string') ? course : user.permissions[ course.id ];
    console.log('provided permissions: ', provided);
    for (const permissionName of required) {
      if (provided[ permissionName ] === true) {
        console.log(`[PERM] Satisfied: ${permissionName}`);
        return true;
      }
    }
    console.log(`[PERM] No satisfied of ${required}`);
    return false;
  }

  public checkAll(user: User, course: Course | string, required: string[]): boolean {
    if (user.is_admin) {
      return true;
    }
    const provided = (typeof course === 'string') ? course : user.permissions[ course.id ];
    console.log('provided permissions: ', provided);
    for (const permissionName of required) {
      if (provided[ permissionName ] !== true) {
        console.log(`[PERM] Not satisfied: ${permissionName}`);
        return false;
      }
    }
    console.log(`[PERM] All satisfied of ${required}.`);
    return true;
  }
}

