import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Project, ProjectConfig } from '../shared/models/models';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProjectService {

  constructor(private http: HttpClient) {
  }

  coursesUrl = `${environment.baseUrl}/api/v1.0/courses`;

  public findProject(courseId: string, projectId: string) {
    const url = `${this.coursesUrl}/${courseId}/projects/${projectId}`;
    console.log(`[PROJECT] Find (${url})`);
    return this.http.get<Project>(url);
  }

  public findProjects(courseId: string): Observable<Project[]> {
    const url = `${this.coursesUrl}/${courseId}/projects`;
    console.log(`[PROJECT] Find (${url})`);
    return this.http.get<Project[]>(url);
  }


  public updateProject(project: Project) {
    const body = {
      name: project.name,
      description: project.description,
      codename: project.codename,
      assignment_url: project.assignment_url,
    };
    const url = `${this.coursesUrl}/${project.course.id}/projects/${project.id}`;
    console.log(`[PROJECT] Update (${url}): `, body);
    return this.http.put(url, body);
  }

  public updateProjectConfig(config: ProjectConfig) {
    const url = `${this.coursesUrl}/${config.project.course.id}/projects/${config.project.id}/config`;
    console.log(`[PROJECT] Update config (${url}):`, config);
    return this.http.put(url, config);
  }

  public deleteProject(courseId: string, projectId: string) {
    const url = `${this.coursesUrl}/${courseId}/projects/${projectId}`;
    console.log(`[PROJECT] Delete (${url})`);
    return this.http.delete(url);
  }

  public createProject(courseId: string, name: string, description: string, codename: string, assignment_url: string) {
    const url = `${this.coursesUrl}/${courseId}/projects`;
    const body = { name: name, description: description, codename: codename, assignment_url: assignment_url };
    console.log(`[PROJECT] Create (${url}): `, body);
    return this.http.post(url, body);
  }

  updateProjectFiles(courseId: string, projectId: string) {
    const url = `${this.coursesUrl}/${courseId}/projects/${projectId}/test-files-refresh`;
    console.log(`[PROJECT] Update project test files (${url})`);
    return this.http.post(url, {});
  }
}
