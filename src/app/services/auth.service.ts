import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

import { environment } from '../../environments/environment';
import { UserService } from './user.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { User } from '../shared/models/models';
import { CookieService } from 'ngx-cookie-service';
import { tap } from 'rxjs/operators';


@Injectable()
export class AuthService implements OnDestroy {
  public redirectUrl: string;
  private loggedInUserSubject = new BehaviorSubject<User>({} as User);
  public loggedInUser = this.loggedInUserSubject.asObservable();
  subscriptions: Subscription[] = [];

  constructor(private http: HttpClient,
              public jwtHelper: JwtHelperService,
              private userService: UserService,
              private cookies: CookieService) {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private setAccessToken(response) {
    const authToken = response[ 'access_token' ];
    if (authToken) {
      localStorage.setItem('access_token', authToken);
    } else {
      console.error('There is no access token:', response);
    }
  }

  private setRefreshToken(response) {
    const refreshToken = response[ 'refresh_token' ];
    if (refreshToken) {
      localStorage.setItem('refresh_token', refreshToken);
    } else {
      console.error('There is no access token:', response);
    }
  }

  public setLoggedInUser(user: User) {
    this.loggedInUserSubject.next(user);
  }

  public setBothTokens(response) {
    this.setAccessToken(response);
    this.setRefreshToken(response);
  }

  // TODO: use token freshness http://flask-jwt-extended.readthedocs.io/en/latest/token_freshness.html
  public hasValidAccessToken() {
    console.log('[AUTH] Access token checking');
    const accessToken = localStorage.getItem('access_token');
    if (accessToken == null) {
      console.log('[AUTH] Access token: NOT FOUND -> FALSE');
      return false;
    }
    const isExpired = this.jwtHelper.isTokenExpired(accessToken);
    if (isExpired) {
      console.log('[AUTH] Access token: EXPIRED -> FALSE');
      return false;
    }
    return true;
  }

  public hasValidRefreshToken() {
    console.log('checking refresh token validity');
    const refreshToken = localStorage.getItem('refresh_token');
    if (refreshToken === null) {
      console.log('[AUTH] Refresh token: NONE  -> FALSE');
      return false;
    }
    if (this.jwtHelper.isTokenExpired(refreshToken)) {
      console.log('[AUTH] Refresh token: EXPIRED -> FALSE');
      return false;
    }
    console.log('[AUTH] Refresh token: VALID -> TRUE');
    return true;
  }

  public refreshAccessToken() {
    console.log('refreshing access token');
    const refreshToken = localStorage.getItem('refresh_token');
    // removes the current access token form local storage, otherwise it gets sent in the authorization header
    localStorage.removeItem('access_token');
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Bearer ' + refreshToken);
    return this.http.post(environment.baseUrl + '/api/v1.0/auth/refresh', {}, { headers: headers }).pipe(
      tap(res => {
        this.setAccessToken(res);
        console.log('[AUTH] Set access token at refresh');
      }));
  }

  public login(params) {
    const response = this.http.post(environment.baseUrl + '/api/v1.0/auth/login', params);
    return response.pipe(
      tap(res => {
        console.log('[AUTH] Setting tokens in pipe');
        this.setBothTokens(res);
        localStorage.setItem('userId', res[ 'id' ]);
      })
    );
  }

  public basicLogin(username: string, password: string) {
    return this.login({ identifier: username, secret: password, type: 'username_password' });
  }

  public oauthLogin(username, oauthToken) {
    return this.login({ identifier: username, secret: oauthToken, type: 'gitlab' });

  }

  public logout() {
    if (this.hasValidAccessToken()) {
      // this.http.post(environment.baseUrl + '/api/v1.0/auth/logout', {});
    }

    this.clearAuthData();
    this.clearCookies();
  }


  public clearAuthData() {
    this.setLoggedInUser({} as User);
    localStorage.clear();
  }

  public clearCookies() {
    this.cookies.deleteAll();
  }

  public async initUser(userId = localStorage.getItem('userId')): Promise<User> {
    if (userId == null || userId === '') {
      console.warn('[AUTH] userId not found in local storage');
      await this.logout();
      return null; // check, make consistent
    }
    const user = await this.userService.getUser(userId).toPromise().catch((error: any) => {
      console.error('[AUTH] Error getting user at user refresh: ', error);
      return null;
    });
    if (user === null) {
      console.warn('[AUTH] no user found: ', user);
      return null;
    }
    console.log('got user: ', user);
    const permissions = await this.loadPermissionsForUser(user).toPromise();
    if (permissions == null) {
      console.warn('[AUTH] No permissions loaded: ', permissions);
      return null;
    }
    console.log('[AUTH] Permissions: ', permissions);
    user.permissions = permissions;
    console.log('[AUTH] Setting logged in user to: ', user);
    this.setLoggedInUser(user);
    return user;
  }

  public getLoggedInUser(): User {
    return this.loggedInUserSubject.value;
  }

  public loadPermissionsForUser(user: User) {
    return this.http.get(environment.baseUrl + '/api/v1.0/users/' + user.id + '/permissions');
  }
}
