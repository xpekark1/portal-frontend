import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Submission, User } from '../shared/models/models';
import { Observable } from 'rxjs';

@Injectable()
export class SubmissionService {

  constructor(private http: HttpClient) {
  }

  submissionsUrl = `${environment.baseUrl}/api/v1.0/submissions`;

  public deleteSubmission(id: string) {
    const url = `${this.submissionsUrl}/${id}`;
    return this.http.delete(url);
  }

  public getSubmission(id: string) {
    const url = `${this.submissionsUrl}/${id}`;
    return this.http.get<Submission>(url);
  }

  public createSubmission(cid: string, pid: string, projectData, fileParams): Observable<Submission> {
    return this.http.post<Submission>(`${environment.baseUrl}/api/v1.0/courses/${cid}/projects/${pid}/submissions`, {
      project_data: projectData,
      file_params: fileParams
    });
  }

  public cancelSubmission(id: string) {
    return this.http.put(`${this.submissionsUrl}/${id}/state`, { state: 'CANCELLED' });
  }

  public resubmitSubmission(id: string, note: string) {
    return this.http.post(`${this.submissionsUrl}/${id}/resubmit`, { note: note });
  }

  public downloadZip(id: string, type: string) {
    const options = { responseType: 'blob' as 'json' };
    return this.http.get<Blob>(`${this.submissionsUrl}/${id}/files/${type}`, options);
  }

  public downloadSingleFile(id: string, type: string, path: string) {
    const options = { params: { path: path } };
    return this.http.get(`${this.submissionsUrl}/${id}/files/${type}`, options);
  }

  public getTree(id: string, type: string) {
    return this.http.get(`${this.submissionsUrl}/${id}/files/${type}/tree`);
  }

  public listSubmissions(config: { user?: string; course?: string; projects?: string[] }) {
    const options = { params: config };
    return this.http.get(this.submissionsUrl, options);
  }

  public submissionStats(id: string) {
    const url = `${this.submissionsUrl}/${id}/stats`;
    return this.http.get(url);
  }
}

