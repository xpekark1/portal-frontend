import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Worker } from '../shared/models/models';
import { WorkerService } from '../services/worker.service';
import { map } from 'rxjs/operators';

@Injectable()
export class WorkersResolver implements Resolve<Worker[]> {
  constructor(public service: WorkerService,
              private router: Router,
              private flashMessagesService: FlashMessagesService) {
  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Worker[]> | Promise<Worker[]> | Worker[] {
    return this.service.listWorkers().pipe(map(workers => {
        if (workers) {
          console.log('Workers: ', workers);
          return workers;
        } else {
          this.router.navigateByUrl('/').then(
            () => {
              this.flashMessagesService.show(`Workers not found.`, { cssClass: 'alert-danger' });
            }, (error: any) => {
              console.error('[RESOLVE] Could not navigate to workers list: ', error.error);
            });
          return null;
        }
      })
    );
  }
}
