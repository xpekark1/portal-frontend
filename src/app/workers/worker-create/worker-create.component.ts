import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { Router } from '@angular/router';
import { WorkerService } from '../../services/worker.service';

@Component({
  selector: 'app-component-create',
  templateUrl: './worker-create.component.html',
  styleUrls: [ './worker-create.component.scss' ]
})
export class WorkerCreateComponent implements OnInit, OnDestroy {

  createData: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private service: WorkerService,
              private router: Router,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService) {
    this.createForm();
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.createData = this.fb.group({
      name: [ '', Validators.required ],
      url: [ '' ],
      portalSecret: [ '' ],
      tags: [ '' ],
    });
  }

  doCreate() {
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    console.log('[CREATE] Worker create form data: ', data);

    this.subscriptions.push(this.service.createWorker(data).subscribe(
      () => {
        this.router.navigateByUrl('/workers').then(() => {
          this.flashMessagesService.show(`Created component ${data[ 'name' ]}.`, { cssClass: 'alert-success' });
        });
      }, (error: any) => {
        console.error('Error creating worker: ', error);
        this.flashMessagesService.show(`Error creating component ${data[ 'name' ]}.`, { cssClass: 'alert-danger', timeout: 10000 });
        console.error('Response data: ', error.error);
        this.flashMessagesService.show(`${error.error.message}`, { cssClass: 'alert-danger', timeout: 10000 });
      }));
  }


}
