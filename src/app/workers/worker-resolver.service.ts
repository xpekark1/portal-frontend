import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Worker } from '../shared/models/models';
import { Observable } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { WorkerService } from '../services/worker.service';
import { map, take } from 'rxjs/operators';


@Injectable()
export class WorkerResolver implements Resolve<Worker> {
  constructor(public service: WorkerService,
              private router: Router,
              private flashMessagesService: FlashMessagesService) {
  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Worker> | Promise<Worker> | Worker {
    const wid = route.paramMap.get('wid');
    return this.service.getWorker(wid).pipe(take(1), map(
      worker => {
        if (worker) {return worker;
        } else {
          this.router.navigateByUrl('/workers').then(
            () => {
              this.flashMessagesService.show(`Worker ${wid} not found.`, { cssClass: 'alert-danger' });
            }, (error: any) => {
              console.error('[RESOLVE] Could not navigate to workers list: ', error.error);
            });
          return null;
        }
      }
    ));
  }
}

