import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkerListComponent } from './worker-list/worker-list.component';
import { WorkerDetailComponent } from './worker-detail/worker-detail.component';
import { WorkerCreateComponent } from './worker-create/worker-create.component';
import { WorkerResolver } from './worker-resolver.service';
import { WorkerRoutingModule } from './worker-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { WorkersResolver } from './workers-resolver.service';
import { WorkerService } from '../services/worker.service';
import { SecretsModule } from '../secrets/secrets.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  imports: [
    CommonModule,
    WorkerRoutingModule,
    ReactiveFormsModule,
    SecretsModule,
    NgxDatatableModule
  ],
  providers: [
    WorkerResolver,
    WorkersResolver,
    WorkerService,
  ],
  declarations: [
    WorkerListComponent,
    WorkerDetailComponent,
    WorkerCreateComponent
  ]
})
export class WorkersModule {
}
