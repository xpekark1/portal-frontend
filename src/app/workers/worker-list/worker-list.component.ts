import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { User, Worker } from '../../shared/models/models';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-worker-list',
  templateUrl: './worker-list.component.html',
  styleUrls: [ './worker-list.component.scss' ]
})
export class WorkerListComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  workers: Worker[];
  subscriptions: Subscription[] = [];

  temp: Worker[];
  loadingIndicator = true;
  reorderable = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;


  constructor(
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, workers: Worker[] }) => {
      this.loggedInUser = data.loggedInUser;
      this.workers = data.workers;
      this.temp = [...data.workers];
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.codename.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.workers = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
