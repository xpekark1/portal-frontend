import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Course, User} from '../../shared/models/models';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {CourseService} from '../../services/course.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  subscriptions: Subscription[] = [];
  constructor(private auth: AuthService,
              private route: ActivatedRoute,
              private courseService: CourseService,
              private userService: UserService,
  ) { }

  ngOnInit() {
    this.subscriptions.push(this.route.parent.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
      console.log('dashboard user set to; ', this.loggedInUser);
      this.checkCourses(this.loggedInUser);
    }));

  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  checkCourses(user: User) {
    if (user.courses == null) {
      console.log(`user\'s courses are ${user.courses}, loading`);
      this.loadCourses(user);
    }
  }

  loadCourses(user: User) {
    if (user.is_admin) {
      console.log('user is admin -> loading');
      this.subscriptions.push(this.courseService.listCourses().subscribe(res => {
        user.courses = res;
        console.log('loaded admin course list');
      }));
    } else {
      console.log('user is not admin -> loading');
      this.subscriptions.push(this.userService.loadCoursesForUser(user).subscribe(res => {
        user.courses = res;
        console.log('set user.courses to ', user.courses);
        this.auth.setLoggedInUser(user);
      }));
      console.log('loaded user course list');
    }
  }
}
