import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {BaseLayoutComponent} from './base-layout/base-layout.component';
import {AuthComponent} from '../auth/auth.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {GitlabOauthResultComponent} from '../auth/gitlab-oauth-result/gitlab-oauth-result.component';
import {UnauthenticatedGuard} from '../guards/unauthenticated.guard';
import {AuthenticatedGuard} from '../guards/authenticated.guard';
import {GitlabLoginGuard} from '../guards/gitlab-login.guard';
import {LoggedInUserResolver} from '../resolvers/logged-in-user-resolver.service';

const appRoutes: Routes = [
  {
    path: '',
    component: BaseLayoutComponent, // https://stackoverflow.com/questions/40508557/multiple-layout-for-different-pages-in-angular-2
    canActivate: [AuthenticatedGuard],
    canActivateChild: [AuthenticatedGuard],
    runGuardsAndResolvers: 'always',
    resolve: {
      loggedInUser: LoggedInUserResolver
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthenticatedGuard],
      },
      {
        path: 'users',
        loadChildren: 'app/users/users.module#UsersModule',
        canLoad: [AuthenticatedGuard] // duplicate check?
      },
      {
        path: 'courses',
        loadChildren: 'app/courses/courses.module#CoursesModule',
        canLoad: [AuthenticatedGuard] // duplicate check?
      },
      {
        path: 'workers',
        loadChildren: 'app/workers/workers.module#WorkersModule',
        canLoad: [AuthenticatedGuard] // duplicate check?
      },
      {
        path: 'submissions',
        loadChildren: 'app/submissions/submissions.module#SubmissionsModule',
        canLoad: [AuthenticatedGuard]
      },
      {
        path: 'secrets',
        loadChildren: 'app/secrets/secrets.module#SecretsModule',
        canLoad: [AuthenticatedGuard] // shouldn't need to route here
      }
    ]
  },
  {
    path: 'gitlabLogin',
    component: GitlabOauthResultComponent,
    canActivate: [UnauthenticatedGuard, GitlabLoginGuard]
  },
  {
    path: 'login',
    component: AuthComponent,
    canActivate: [UnauthenticatedGuard]
  },
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class CoreRoutingModule {
}
