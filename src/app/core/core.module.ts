import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { RouterModule } from '@angular/router';
import { FlashMessagesModule } from 'angular2-flash-messages';


import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthModule } from '../auth/auth.module';
import { CoreRoutingModule } from './core-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CourseService } from '../services/course.service';
import { BaseLayoutComponent } from './base-layout/base-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { environment } from '../../environments/environment';
import { UnauthenticatedGuard } from '../guards/unauthenticated.guard';
import { AuthenticatedGuard } from '../guards/authenticated.guard';
import { GitlabLoginGuard } from '../guards/gitlab-login.guard';
import { AdminGuard } from '../guards/admin.guard';
import { AdminOrOwnGuard } from '../guards/admin-or-own.guard';
import { ProjectService } from '../services/project.service';
import { LoggedInUserResolver } from '../resolvers/logged-in-user-resolver.service';
import { PermissionsService } from '../services/permissions.service';
import { SubmissionService } from '../services/submission.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { CourseQueryParamResolver } from '../resolvers/course-query-param-resolver.service';
import { ProjectQueryParamResolver } from '../resolvers/project-query-param-resolver.service';
import { WorkerService } from '../services/worker.service';
import { SecretsModule } from '../secrets/secrets.module';
import { SecretService } from '../services/secret.service';
import { ClientService } from '../services/client.service';
import { ManagementService } from '../services/management.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlashMessagesModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [environment.baseDomain],
        blacklistedRoutes: [environment.baseDomain + '/api/v1.0/auth/login', environment.baseDomain + '/api/v1.0/auth/refresh']
      }
    }),
    AuthModule,
    CoreRoutingModule,
    AngularMultiSelectModule,
    SharedModule,
    SecretsModule
  ],
  declarations: [
    PageNotFoundComponent,
    BaseLayoutComponent,
    DashboardComponent
  ],
  providers: [
    AuthService,
    UserService,
    WorkerService,
    CourseService,
    ProjectService,
    SecretService,
    ClientService,
    PermissionsService,
    SubmissionService,
    ManagementService,
    AuthenticatedGuard,
    UnauthenticatedGuard,
    GitlabLoginGuard,
    AdminGuard,
    AdminOrOwnGuard,
    LoggedInUserResolver,
    CourseQueryParamResolver,
    ProjectQueryParamResolver,
    DatePipe
  ],
  exports: [
    RouterModule
  ]
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only.');
    }
  }
}

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

// sources:
// https://www.technouz.com/4644/angular-5-app-structure-multiple-modules/
// https://medium.com/@tomastrajan/6-best-practices-pro-tips-for-angular-cli-better-developer-experience-7b328bc9db81
// https://angular.io/guide/styleguide#!#application-structure-and-angular-modules
