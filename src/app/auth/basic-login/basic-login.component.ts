import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {FlashMessagesService} from 'angular2-flash-messages/module/flash-messages.service';
import {catchError, map} from 'rxjs/operators';
import {of, Subscription} from 'rxjs';

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  subscriptions: Subscription[] = [];

  constructor(private auth: AuthService,
              private router: Router,
              private userService: UserService,
              private flashMessagesService: FlashMessagesService) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(),
      password: new FormControl()
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  doLogin() {
    const data = this.loginForm.value;

    this.subscriptions.push(this.auth.basicLogin(data['username'], data['password']).pipe(
      map(() => {
          this.router.navigateByUrl('/dashboard').then(() => {
            this.flashMessagesService.show('Login successful.', {cssClass: 'alert-success'});
          });
        }
      ),
      catchError(error => {
        console.log(`[LOGIN] Basic, username ${data['username']}, password ${data['password']} error: `, error);
        this.loginForm.reset();
        this.flashMessagesService.show('Incorrect username or password.', {cssClass: 'alert-danger'});
        return of(null);
      })
    ).subscribe());
  }
}
