import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicLoginComponent } from './basic-login/basic-login.component';
import { AuthComponent } from './auth.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { GitlabOauthResultComponent } from './gitlab-oauth-result/gitlab-oauth-result.component';
import { CookieService } from 'ngx-cookie-service';
import {FlashMessagesModule} from 'angular2-flash-messages';
import { GitlabLoginComponent } from './gitlab-login/gitlab-login.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlashMessagesModule.forRoot()
  ],
  declarations: [
    BasicLoginComponent,
    AuthComponent,
    GitlabOauthResultComponent,
    GitlabLoginComponent
  ],
  exports: [
    BasicLoginComponent,
    AuthComponent
  ],
  providers: [
    AuthService,
    CookieService
  ],
})

export class AuthModule {
}
