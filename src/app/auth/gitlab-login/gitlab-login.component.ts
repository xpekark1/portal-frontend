import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-gitlab-login',
  templateUrl: './gitlab-login.component.html',
  styleUrls: ['./gitlab-login.component.scss']
})
export class GitlabLoginComponent implements OnInit {

  backendUrl: string = environment.baseUrl + '/api/v1.0/oauth/login';

  constructor() { }

  ngOnInit() {
  }

  doGitlabLogin() {
    window.location.href = this.backendUrl;
  }

}
