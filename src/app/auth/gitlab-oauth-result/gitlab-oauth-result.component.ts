import {Component, OnDestroy, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {AuthService} from '../../services/auth.service';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import {catchError, map} from 'rxjs/operators';
import {of, Subscription} from 'rxjs';

@Component({
  selector: 'app-gitlab-oauth-result',
  templateUrl: './gitlab-oauth-result.component.html',
  styleUrls: ['./gitlab-oauth-result.component.scss']
})
export class GitlabOauthResultComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];

  constructor(
    private cookies: CookieService,
    private auth: AuthService,
    private userService: UserService,
    private router: Router,
    private flashMessagesService: FlashMessagesService
  ) {
  }

  ngOnInit() {
    console.log('processing gitlab oauth login');
    this.doLogin();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  doLogin() {
    const gitlab_token = this.cookies.get('gitlab_token');
    const username = this.cookies.get('username');
    console.log('Gitlab username: ', username);
    console.log('Gitlab token: ', gitlab_token);
    this.subscriptions.push(this.auth.oauthLogin(username, gitlab_token).pipe(
      map(() => {
        this.router.navigateByUrl('/dashboard').then(
          () => {
            this.flashMessagesService.show('Login successful.', {cssClass: 'alert-success'});
          });
      }),
      catchError(error => {
        console.log(`[LOGIN] GitLab, username ${username}, token ${gitlab_token} error: `, error);
        this.flashMessagesService.show('OAuth login using GitLab failed.', {cssClass: 'alert-danger'});
        return of(null);
      })).subscribe());
  }

}
