import { TestBed, inject } from '@angular/core/testing';

import { SubmissionResolver } from './submission-resolver.service';

describe('SubmissionResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubmissionResolver]
    });
  });

  it('should be created', inject([SubmissionResolver], (service: SubmissionResolver) => {
    expect(service).toBeTruthy();
  }));
});
