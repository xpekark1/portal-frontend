import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Course, Submission, User } from '../../shared/models/models';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { PermissionsService } from '../../services/permissions.service';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';
import { AuthService } from '../../services/auth.service';
import { SubmissionService } from '../../services/submission.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-submission-list',
  templateUrl: './submission-list.component.html',
  styleUrls: [ './submission-list.component.scss' ]
})
export class SubmissionListComponent implements OnInit, OnDestroy {
  loading: boolean;
  loggedInUser: User;
  filters: FormGroup;
  subscriptions: Subscription[] = [];

  @ViewChild('myTable') table: any;

  courseOptions = [];
  groupOptions = [];
  roleOptions = [];
  projectOptions = [];
  stateOptions = [];

  courseSettings: any = { singleSelection: true, text: 'Select Course' };
  groupSettings: any = { singleSelection: false, text: 'Select Group' };
  roleSettings: any = { singleSelection: false, text: 'Select Role' };
  projectSettings: any = { singleSelection: false, text: 'Select Project' };
  stateSettings: any = { singleSelection: true, text: 'Select State' };
  resultSettings: any = { singleSelection: true, text: 'Select Result' };

  selectedCourse: Course = null;
  selectedGroups = [];
  selectedRoles = [];
  selectedProjects = [];
  selectedUser: string = null;
  selectedState;
  temp: Submission[];
  submissions: Submission[];
  resultOptions = [];
  selectedResult: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private permissions: PermissionsService,
              private userService: UserService,
              private courseService: CourseService,
              private submissionService: SubmissionService,
              private flashMessagesService: FlashMessagesService,
              private auth: AuthService) {
    this.createForm();
    this.loading = true;
  }

  static optionsFromArray(arr: string[]) {
    return arr.map((x) => ({ id: x, itemName: x }));
  }

  async ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User }) => {
      this.loggedInUser = data.loggedInUser;
      this.loadSubmissionsForUser();
    }));
    await this.initCourseOptions();
    this.loading = false;
    this.stateOptions = SubmissionListComponent.optionsFromArray([ 'FINISHED', 'CREATED', 'CANCELLED', 'ABORTED', 'IN_PROGRESS' ]);
    this.resultOptions = SubmissionListComponent.optionsFromArray([ 'pass', 'fail', 'error', 'skip', 'none' ]);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  createForm() {
    this.filters = this.fb.group({
      course: [ [], Validators.required ],
      group: [ [] ],
      role: [ [] ],
      project: [ [] ]
    });
  }

  async initCourseOptions() {
    console.log('user courses: ', this.loggedInUser.courses);
    if (this.loggedInUser.courses == null || this.loggedInUser.courses) {
      await this.userService.loadCoursesForUser(this.loggedInUser).toPromise().then(courses => {
        this.loggedInUser.courses = courses;
        this.auth.setLoggedInUser(this.loggedInUser);
        this.loggedInUser.courses.forEach((item, index) => {
          this.courseOptions.push({ 'id': item.id, 'itemName': item.codename });
        });
      });
    }
  }

  initFormOptions() {
    const selectedCourse = this.selectedCourse;
    this.courseService.loadGroupsForCourse(selectedCourse).subscribe(groups => {
      groups.forEach((item, index) => {
        this.groupOptions.push({ 'id': item.id, 'itemName': item.name });
      });
      console.log('set group options to ', this.groupOptions);
    });
    this.courseService.loadRolesForCourse(selectedCourse).subscribe(roles => {
      roles.forEach((item, index) => {
        this.roleOptions.push({ 'id': item.id, 'itemName': item.name });
      });
      console.log('set role options to ', this.roleOptions);
    });
    this.courseService.loadProjectsForCourse(selectedCourse).subscribe(projects => {
      projects.forEach((item, index) => {
        this.projectOptions.push({ 'id': item.id, 'itemName': item.name });
      });
      console.log('set project options to ', this.projectOptions);
    });
  }

  showFiltered() {
    this.loadSubmissionsForUser();
  }

  onCourseSelect(course: Course) {
    console.log('selected course ', course);
    this.selectedCourse = course;
    this.resetGroupAndRoleOptions();
    this.initFormOptions();
  }

  onCourseDeselect(course: Course) {
    console.log('deselected course ', course);
    this.selectedCourse = null;
    this.resetGroupAndRoleOptions();
  }

  resetGroupAndRoleOptions() {
    this.groupOptions = [];
    this.selectedGroups = [];
    this.roleOptions = [];
    this.selectedRoles = [];
    this.projectOptions = [];
    this.selectedProjects = [];
    this.loggedInUser.submissions = [];
  }

  loadSubmissionsForUser() {
    const projects = [];
    this.selectedProjects.forEach(({ id }) => {
      projects.push(id);
    });
    // let params = { user: this.selectedUser, course: this.courseId, projects: projects };
    const params = {};
    if (this.selectedUser) {
      params[ 'user' ] = this.selectedUser;
    }
    if (this.selectedCourse) {
      params[ 'course' ] = this.selectedCourse.id;
    }

    if (projects) {
      params[ 'projects' ] = projects;
    }

    this.subscriptions.push(this.submissionService.listSubmissions(params).pipe(
      map(
        (submissions: Submission[]) => {
          const realSubm = submissions.map((item) => {
              item[ 'username' ] = item.user.username;
              return item;
            }
          );
          console.log('in map, got submissions: ', realSubm);
          this.submissions = submissions;
          this.temp = [ ...submissions ];
        }
      ), catchError((error: any) => {
        if (error.status === 403) {
          console.log('in error handler: ', error);
          this.submissions = [];
          return [];
        }
      })).subscribe());

  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.user.username.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.submissions = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  onAnySelect(event, selector) {
    const val = event.id.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d[ selector ].toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.submissions = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  onStateSelect(event: any) {
    this.onAnySelect(event, 'state');
  }

  onResultSelect(event: any) {
    this.onAnySelect(event, 'result');
  }

  onAnyDeselect(event: any) {
    this.submissions = [ ...this.temp ];
  }

  getRowClass = (row: any) => {
    const rowResult = row.result;
    const classResultName = `row-class-${rowResult}`;
    const result = {};
    result[ classResultName ] = true;
    return result;
  }
}
