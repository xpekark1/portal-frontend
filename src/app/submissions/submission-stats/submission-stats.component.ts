import { Component, OnDestroy, OnInit } from '@angular/core';
import { Submission, User } from '../../shared/models/models';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SubmissionService } from '../../services/submission.service';
import { PermissionsService } from '../../services/permissions.service';

@Component({
  selector: 'app-submission-stats',
  templateUrl: './submission-stats.component.html',
  styleUrls: ['./submission-stats.component.scss']
})
export class SubmissionStatsComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  submission: Submission;
  subscriptions: Subscription[] = [];
  stats: any;
  objectKeys = Object.keys;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private flashMessagesService: FlashMessagesService,
              private service: SubmissionService,
              private permissions: PermissionsService
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, submission: Submission }) => {
      if (data.submission == null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`Not permitted to view requested submission.`, { cssClass: 'alert-danger' });
        });
      }
      this.loggedInUser = data.loggedInUser;
      this.submission = data.submission;
      this.subscriptions.push(this.service.submissionStats(this.submission.id).subscribe((stats) => {
        console.log('[STATS] Stats: ', stats);
        this.stats = stats;
      }));
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
