import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Submission, User } from '../../shared/models/models';
import { Subscription } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SubmissionService } from '../../services/submission.service';
import { PermissionsService } from '../../services/permissions.service';

@Component({
  selector: 'app-submission-detail',
  templateUrl: './submission-detail.component.html',
  styleUrls: [ './submission-detail.component.scss' ]
})
export class SubmissionDetailComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  submission: Submission;
  subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private flashMessagesService: FlashMessagesService,
              private service: SubmissionService,
              private permissions: PermissionsService
  ) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.data.subscribe((data: { loggedInUser: User, submission: Submission }) => {
      if (data.submission == null) {
        this.router.navigateByUrl('/').then(() => {
          this.flashMessagesService.show(`Not permitted to view requested submission.`, { cssClass: 'alert-danger' });
        });
      }
      this.loggedInUser = data.loggedInUser;
      this.submission = data.submission;
      console.log('[SUB] Submission: ', this.submission);
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  deleteSubmission() {
    this.service.deleteSubmission(this.submission.id).subscribe(() => {
      this.router.navigateByUrl(`/`).then(() => {
        this.flashMessagesService.show(`Deleted submission ${this.submission.id}.`, { cssClass: 'alert-danger' });
      });
    });
  }

  cancelSubmission() {
    this.subscriptions.push(this.service.cancelSubmission(this.submission.id).subscribe(
      () => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([ '.' ], { relativeTo: this.route }).then(
          () => {
            this.flashMessagesService.show('Submission cancelled.', { cssClass: 'alert-success' });
          }, (error: any) => {
            console.log('Navigation failed: ', error);
            this.flashMessagesService.show('Navigation failed, try navigating elsewhere.', { cssClass: 'alert-danger' });
          }
        );
      }
    ));
  }

  resubmitSubmission() {
    const note = `Resubmitted at ${Date.now()} by ${this.loggedInUser.username} (id ${this.loggedInUser.id})`;
    this.subscriptions.push(this.service.resubmitSubmission(this.submission.id, note).subscribe(
      (submission: Submission) => {
        this.router.navigateByUrl(`/submissions/${submission.id}`).then(
          () => {
            this.flashMessagesService.show('Resubmit successful.', { cssClass: 'alert-success' });
          }, (error: any) => {
            console.log('Navigation failed: ', error);
            this.flashMessagesService.show('Navigation failed, try navigating elsewhere.', { cssClass: 'alert-danger' });
          }
        );
      }
    ));
  }

  userCanCancelSubmission(): boolean {
    return this.loggedInUser.is_admin || this.loggedInUser.id === this.submission.user.id;
  }

  userCanResubmitSubmission(): boolean {
    return this.permissions.checkAll(this.loggedInUser, this.submission.course, [ 'resubmit_submissions' ]);
  }

  public downloadZip(type: string) {
    const subscription = this.service.downloadZip(this.submission.id, type).subscribe(res => {
      this.downloadFile(res, type);
    });
    this.subscriptions.push(subscription);
  }

  public downloadFile(res: any, type: string) {
    const dataType = res.type;
    const binaryData = [];
    binaryData.push(res);
    const link = document.createElement('a');
    const blob = new Blob(binaryData, { type: dataType });
    const url = window.URL.createObjectURL(blob);
    link.href = url;
    link.download = `${this.submission.id}-${type}.zip`;
    document.body.appendChild(link);
    link.click();
    window.URL.revokeObjectURL(url);
  }

  userCanDownloadTestFiles(): boolean {
    return this.permissions.checkReadResults(this.loggedInUser, this.submission.course);
  }

  userCanDownloadResults(): boolean {
    return this.userCanViewResults() && this.submissionHasResultsAvailable();
  }

  submissionHasResultsAvailable(): boolean {
    return this.submission.state === 'FINISHED';
  }

  userCanViewResults() {
    return this.permissions.checkReadResults(this.loggedInUser, this.submission.course);
  }

  userCanViewSources() {
    return true;
  }
}
