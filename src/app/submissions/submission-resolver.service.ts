import { Injectable } from '@angular/core';
import { Submission } from '../shared/models/models';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { SubmissionService } from '../services/submission.service';
import { map, take } from 'rxjs/operators';

@Injectable()
export class SubmissionResolver implements Resolve<Submission> {

  constructor(private submissionService: SubmissionService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Submission> | Promise<Submission> | Submission {
    const sid = route.paramMap.get('sid');

    return this.submissionService.getSubmission(sid).pipe(take(1), map(course => {
        if (course) {
          return course;
        } else {
          console.log('submission get failed');
          return null;
        }
      })
    );

  }

}
