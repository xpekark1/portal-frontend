import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Course, Project, Submission, User } from '../../shared/models/models';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages/module/flash-messages.service';
import { PermissionsService } from '../../services/permissions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { CourseService } from '../../services/course.service';
import { SubmissionService } from '../../services/submission.service';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-submission-create',
  templateUrl: './submission-create.component.html',
  styleUrls: [ './submission-create.component.scss' ]
})


export class SubmissionCreateComponent implements OnInit, OnDestroy {
  loggedInUser: User;
  createData: FormGroup;
  subscriptions: Subscription[] = [];

  courseOptions = [];
  projectOptions = [];
  typeOptions = [ 'git' ];
  selectedProject;
  selectedCourse;

  constructor(private router: Router,
              private route: ActivatedRoute,
              public auth: AuthService,
              private fb: FormBuilder,
              private flashMessagesService: FlashMessagesService,
              private permissions: PermissionsService,
              private userService: UserService,
              private courseService: CourseService,
              private submissionService: SubmissionService) {
    this.createForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngOnInit() {
    const handleRouteData = (data: { loggedInUser: User, course: Course, project: Project }) => {
      this.loggedInUser = data.loggedInUser;
      this.selectedCourse = data.course;
      this.selectedProject = data.project;
      console.log('Preloaded route data: ', this.selectedCourse, this.selectedProject);
      this.loadCoursesForSubmission();
    };
    this.subscriptions.push(this.route.data.subscribe(handleRouteData));
  }

  private loadCoursesForSubmission() {
    this.subscriptions.push(
      this.loadCourseOptions().pipe(map((courses) => {
        this.courseOptions = courses;
        this.selectedCourse = this.checkCourseInOptions();
        if (this.selectedCourse) {
          this.loadProjectsBasedOnCourse();
        }
      }), catchError((error: any) => {
        console.error('Error loading course options: ', error);
        this.flashMessagesService.show('Failed to load course options!', { cssClass: 'alert-warning' });
        return error;
      })).subscribe()
    );
  }

  loadProjectsBasedOnCourse() {
    this.subscriptions.push(
      this.loadProjectOptions(this.selectedCourse).pipe(map((projects) => {
        this.projectOptions = projects;
        this.selectedProject = this.projectInProjectOptions(this.selectedProject);
        this.initFormValues();

      }), catchError((error: any) => {
        console.error(`Error loading project options for course ${this.selectedCourse}: `, error);
        this.flashMessagesService.show('Failed to load project options!', { cssClass: 'alert-warning' });
        return error;
      })).subscribe()
    );
  }

  checkCourseInOptions(): Course {
    if (this.courseOptions.length > 0 && this.selectedCourse == null) {
      return this.courseOptions[ 0 ];
    }
    return this.courseOptions.find((c) => c.id === this.selectedCourse.id);
  }

  projectInProjectOptions(project: Project): Project {
    if (!project && this.projectOptions.length > 0) {
      return this.projectOptions[ 0 ];
    }
    return this.projectOptions.find((p) => p.id === project.id);
  }

  initFormValues() {
    console.log('[FORM] Init: ', this.selectedCourse, this.selectedProject);
    this.createData.patchValue({
      course: this.selectedCourse,
      project: this.selectedProject,
      url: `git@gitlab.fi.muni.cz:${this.loggedInUser.username}/${this.selectedCourse.codename}.git`,
      from_dir: this.selectedProject.codename,
    }, { emitEvent: false, onlySelf: true });
  }

  createForm() {
    this.createData = this.fb.group({
      course: [ null, Validators.required ],
      project: [ null, Validators.required ],
      type: [ 'git', Validators.required ],
      from_dir: '',
      url: '',
      branch: 'master',
      checkout: ''
    });
  }

  loadCourseOptions(): Observable<Course[]> {
    return this.userService.loadCoursesForUser(this.loggedInUser).pipe(
      map(courses => {
        return courses.filter((c) => this.loggedInUser.permissions[ c.id ][ 'create_submissions' ]);
      }));
  }

  loadProjectOptions(course: Course): Observable<Project[]> {
    console.log(`loading project options for course ${course.codename}.`);
    this.projectOptions = [];
    this.createData[ 'project' ] = null;
    return this.courseService.loadProjectsForCourse(course).pipe(
      map(projects => {
        this.projectOptions = projects;
        return projects;
      })
    );
  }

  selectProjectForCourse(course: Course) {
    this.subscriptions.push(
      this.loadProjectOptions(course).pipe(map((projects) => {
        this.selectedCourse = course;
        if (projects.length > 0) {
          if (!this.selectedProject) {
            this.selectedProject = projects[ 0 ];
          }
          this.createData.patchValue({
            project: projects[ 0 ],
            url: `git@gitlab.fi.muni.cz:${this.loggedInUser.username}/${this.selectedCourse.codename}.git`,
          }, { emitEvent: true, onlySelf: true });
        }
      }), catchError((error: any) => {
        console.error(`Error loading project options for course ${course}: `, error);
        this.flashMessagesService.show('Failed to load project options!', { cssClass: 'alert-warning' });
        return error;
      })).subscribe()
    );
  }

  validateGitData(): boolean {
    const data = this.createData.value;
    this.createData.updateValueAndValidity();
    if (data[ 'type' ] === 'git') {
      if (data[ 'url' ] == null || data[ 'url' ] === '') {
        return false;
      }
    }
    return true;
  }

  doCreate() {
    // TODO: project data (same as backend)
    const data = this.createData.value;
    if (this.createData.status !== 'VALID') {
      console.log('invalid form submitted');
      return;
    }
    console.log('Submission create form data: ', data);
    const courseId = data[ 'course' ].id;
    const projectId = data[ 'project' ].id;
    const fileParams = {
      source: {
        type: data[ 'type' ],
      },
      from_dir: data[ 'from_dir' ]
    };
    if (data[ 'type' ] === 'git') {
      fileParams.source[ 'url' ] = data[ 'url' ];
      fileParams.source[ 'branch' ] = data[ 'branch' ];
      fileParams.source[ 'checkout' ] = data[ 'checkout' ];
    }
    const handleError = (error: any) => {
      console.error('Error creating submission: ', error);
      this.flashMessagesService.show(`Error creating submission: ${error.error.message}.`, { cssClass: 'alert-danger', timeout: 10000 });
      this.flashMessagesService.show(`You can create new submission: ${error.error.message[ 'when' ]}.`, {
        cssClass: 'alert-danger',
        timeout: 30000
      });
    };

    const handleSubmission = (submission: Submission) => {
      this.router.navigateByUrl(`/submissions/${submission.id}`).then(() => {
        this.flashMessagesService.show(`Submission created successfully.`, { cssClass: 'alert-success' });
      });
    };
    this.subscriptions.push(
      this.submissionService.createSubmission(courseId, projectId, {}, fileParams)
        .subscribe(handleSubmission, handleError)
    );
  }

  projectChanged(event) {
    this.selectedProject = event;
    this.createData.patchValue({
      from_dir: this.selectedProject.codename
    }, { emitEvent: true, onlySelf: true });
  }
}
